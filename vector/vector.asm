	ballxpos0 = ram_start
        balldiffx0 = ram_start+1
        balldiffy0 = ram_start+2
        ballvalue0 = ram_start+3
        balldir0 = ram_start+4
        balldiffycount = ram_start+5
        colorpointer = ram_start+6
	zero = ram_start+8
        dest_x = ram_start+10
        dest_y = ram_start+11
        current_x = ram_start+12
        current_y = ram_start+13
        coord_pointer = ram_start+14
        screenram = ram_start+16
        SCREENRAMSIZE = 4*16
        
!macro linemove {
	lda ballvalue0
	sec
        sbc balldiffx0
        bcs +
        adc balldiffy0
	sta ballvalue0
        lda screenram+2,x
        sta PF2
        lda screenram+3,x
        sta PF1
        lda balldir0
	sta HMM1
        bne ++        
+       sta ballvalue0
	nop
        lda screenram+2,x
        sta PF2
        lda screenram+3,x
        sta PF1
        nop
        nop
	lda #$00
	sta HMM1
++
}
vector:
	lda initflag
        bne vecnoinit
	lda #128           ;16 measures, lines up nicely
	sta part_duration
        lda #<(coord_list+2)
        sta coord_pointer
        lda #>(coord_list+2)
        sta coord_pointer+1
        lda #$58
        sta ballxpos0
        sta current_x
        lda coord_list
        sta dest_x
        lda #$0
        sta current_y
        lda coord_list+1
        sta dest_y
        lda #<colortab
        sta colorpointer
        lda #>colortab
        sta colorpointer+1
        lda #$00
        sta zero
        ;sta COLUP0
        ;sta GRP1
        ldx #SCREENRAMSIZE-1
-	sta screenram,x
	dex
        bpl -
        lda #$10
        sta NUSIZ1
        lda #$3f
        sta COLUP1
        lda #$68
        sta COLUPF
        lda #$01
        sta CTRLPF
        ldx #0
        lda #$a4
        jsr PosObject
        lda #$7f
        sta GRP0
        ;lda #$00
        ;sta HMP0
        lda #$05
        sta NUSIZ0 

vecnoinit:
	;handling of line
        lda dest_x
        cmp current_x
        bne make_normal_move
        lda dest_y
        cmp current_y
        bne make_normal_move
        lda current_x
        sec
        sbc #$18
        lsr
        lsr
        tay
        lda current_y
        sec
        sbc #121
        lsr
        lsr
        lsr
        tax
        jsr plot
        ldy #$00
        lda (coord_pointer),y
        sta dest_x
        iny
	lda (coord_pointer),y
        sta dest_y
        ora dest_y
        bne +
+       lax coord_pointer
        sbx #$fe
        stx coord_pointer
        bcc +
        inc coord_pointer+1
+
make_normal_move:
	lda dest_y
        cmp current_y
        beq make_no_y
        bcs make_inc_y
        dec current_y
        !byte $2c
make_inc_y:
	inc current_y
make_no_y:        

	lda dest_x
        cmp current_x
        beq make_no_x
        bcs make_inc_x
        dec current_x
        !byte $2c
make_inc_x:
	inc current_x
make_no_x:        
        
        lda current_y
        clc
        adc #106
        sta balldiffy0
        ldy #$f0
        lda current_x
        sec
        sbc ballxpos0
        bcs +
        eor #$ff
        adc #$00
        ldy #$10
+	sta balldiffx0        
        sty balldir0
	lda #$00
	sta COLUBK
        sta PF0
        sta PF1
        sta PF2
        lda #227
        sec
        sbc balldiffy0
        sta balldiffycount
        jmp veccontinue

       	+align256
veccontinue:
	jsr WaitForVblankEnd
        ldx #3
        lda ballxpos0
        jsr PosObject
        lda balldiffy0
        lsr
        sta ballvalue0
        ldy #227
        sta WSYNC
        sta HMOVE
        lda #$02
        sta ENAM1
        lda #$00
        sta ENAM0
        sta ENABL
        ldx diff8,y
        nop
        bit $ea

-	+linemove
        ldx diff8,y
        lda (colorpointer),y
	sta COLUBK
        sta HMOVE
        lda screenram,x
        sta PF1
        lda screenram+1,x
        sta PF2
        dey
        cpy balldiffycount
        bne -
        nop
        lda #$00
-       sta ENAM0
        sta ENAM1
        lda screenram+3,x
        sta PF1
        lda screenram+2,x
        sta PF2
        ldx diff8,y        
        lda (colorpointer),y
 	sta WSYNC
	sta COLUBK
	sta HMOVE
        lda screenram,x
        sta PF1
        lda screenram+1,x
        sta PF2
        lda zero
        dey
        bne -
        sta PF1
        sta PF2
        rts

plot:
	;in x y
        ;in y x
        tya
        lsr
        lsr
        asr #$03*2
        sta tmp1
        txa
        asl
        asl
        adc tmp1
        and #$3f
        tax
        tya
        and #$1f
        tay
        cpx #$04
        bcc +
        lda screenram,x
        eor plotbittab,y
        sta screenram,x
+       rts

plotbittab:
	!byte $80,$40,$20,$10,$08,$04,$02,$01
        !byte $01,$02,$04,$08,$10,$20,$40,$80
	!byte $80,$40,$20,$10,$08,$04,$02,$01
        !byte $01,$02,$04,$08,$10,$20,$40,$80
        
	+align256
        
diff8:
	!for i, 128 {
        !byte (128-i)/8*4
        }
	!for i, 128 {
        !byte 0
        }

colortab:
	!src "vector/background.asm"
	!for i, 128 {
        !byte 0
        }
	!macro coord .x, .y {
        !byte .x * 4 + $18 
        !byte .y * 8
        }
        

coord_list:
	!set DSS = 0
	+coord DSS+0,3
	+coord DSS+0,4
	+coord DSS+0,5
	+coord DSS+0,6
	+coord DSS+0,7
	+coord DSS+0,8
	+coord DSS+0,9
	+coord DSS+0,10
	+coord DSS+0,11
	+coord DSS+0,12
	+coord DSS+0,13
	+coord DSS+1,13
	+coord DSS+2,13
	+coord DSS+3,13
	+coord DSS+4,12
	+coord DSS+5,11
	+coord DSS+6,10
	+coord DSS+6,9
	+coord DSS+6,8
	+coord DSS+6,7
	+coord DSS+6,6
	+coord DSS+5,5
	+coord DSS+4,4
	+coord DSS+3,3
	+coord DSS+2,3
	+coord DSS+1,3

	!set DSS = -6
        +coord DSS+18,13
        +coord DSS+19,13
        +coord DSS+20,13
        +coord DSS+21,13
        +coord DSS+22,13
        +coord DSS+23,12
        +coord DSS+24,11
        +coord DSS+24,10
        +coord DSS+23,9
        +coord DSS+22,8
        +coord DSS+21,8
        +coord DSS+20,8
        +coord DSS+19,7
        +coord DSS+18,6
        +coord DSS+18,5
        +coord DSS+19,4
        +coord DSS+20,3
        +coord DSS+21,3
        +coord DSS+22,3
        +coord DSS+23,3
        +coord DSS+24,3

	!set DSS = 6
        +coord DSS+18,13
        +coord DSS+19,13
        +coord DSS+20,13
        +coord DSS+21,13
        +coord DSS+22,13
        +coord DSS+23,12
        +coord DSS+24,11
        +coord DSS+24,10
        +coord DSS+23,9
        +coord DSS+22,8
        +coord DSS+21,8
        +coord DSS+20,8
        +coord DSS+19,7
        +coord DSS+18,6
        +coord DSS+18,5
        +coord DSS+19,4
        +coord DSS+20,3
        +coord DSS+21,3
        +coord DSS+22,3
        +coord DSS+23,3
        +coord DSS+24,3

	+coord 31,13
	+coord 31-12,13
	+coord 31-12-13,13

        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0


	!ifdef COMMENTED_OUT {
	+coord 0,3
        +coord 1,3
        +coord 2,3
        +coord 3,3
        +coord 4,3
        +coord 5,3
        +coord 6,3
        +coord 7,3
        +coord 8,3
        +coord 4,4
        +coord 4,5
        +coord 4,6
        +coord 4,7
        +coord 4,8
        +coord 4,9
        +coord 4,10
        +coord 4,11
        +coord 4,12
        +coord 4,13
        
        +coord 10,13
        +coord 10,12
        +coord 10,11
        +coord 10,10
        +coord 10,9
        +coord 10,8
        +coord 10,7
        +coord 10,6
        +coord 10,5
        +coord 10,4
        +coord 10,3
        +coord 11,3
        +coord 12,3
        +coord 13,3
        +coord 14,3
        +coord 15,4
        +coord 16,5
        +coord 16,6
        +coord 15,7
        +coord 14,8
        +coord 13,8
        +coord 12,8
        +coord 11,8
        +coord 15,9
        +coord 16,10
        +coord 16,11
        +coord 16,12
        +coord 16,13
        
        +coord 18,13
        +coord 19,13
        +coord 20,13
        +coord 21,13
        +coord 22,13
        +coord 23,12
        +coord 24,11
        +coord 24,10
        +coord 23,9
        +coord 22,8
        +coord 21,8
        +coord 20,8
        +coord 19,7
        +coord 18,6
        +coord 18,5
        +coord 19,4
        +coord 20,3
        +coord 21,3
        +coord 22,3
        +coord 23,3
        +coord 24,3
        
        +coord 26,3
        +coord 27,3
        +coord 28,3
        +coord 29,3
        +coord 30,3
        +coord 28,4
        +coord 28,5
        +coord 28,6
        +coord 28,7
        +coord 28,8
        +coord 28,9
        +coord 28,10
        +coord 28,11
        +coord 28,12
        +coord 30,13
        +coord 29,13
        +coord 28,13
        +coord 27,13
        +coord 26,13
                
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
        +coord 0,0
        +coord 31,0
	}
        
        
