
!macro skip_framewait {
        tsx
        lda $03,x
        clc
        adc #FrameWait_Cont-FrameWait
        sta $03,x
        bcc +
        inc $04,x
+	
	}

!macro jsr .code {
        lda #>(.cont-1)
        pha
        lda #<(.cont-1)
        pha
        lda #>(jsr_far_ret-1)
        pha
        lda #<(jsr_far_ret-1)
        pha
        ldx #<.code
        lda #>.code
	jmp jsr_far
.cont:
	}
        
!macro jmp .code {
	ldx #<.code
	lda #>.code
	jmp jsr_far
        
	}        

	!macro align256 {
        * = (*+$ff) & $ff00
        }

	!macro align128 {
        * = (*+$ff) & $ff00
        }
        
        !macro sprdoubleline .x {
        !byte >.x
        * = *+$ff
        !byte <.x
        * = *-$100
        }
        
        !macro make_vectors {
	!set old_pc = *
        * = * & $e000 | $1ffc
	!word jump_to_start
	!word jump_to_start
	}
        	
