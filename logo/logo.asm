	logo_stackmerk = tmp1
	logo_gfx_stack = ram_start

	LOFFSET = logo_gfx_stack-1

L_NOP = $2f

        !macro logo_poke .x, .y, .a, .b {
        !byte .x
        * = *+$7f
        !byte .y
        * = *+$7f
        !byte .a
        * = *+$7f
        !byte .b
        * = *-$180
        }
	+align256
	!for i, $43 {
	!byte 0
	}
logo:
	lda initflag
	bne logo_noinit
	!ifdef enable_scroll {
	} else {
	lda #$00
	sta initflag_high
	}
logo_noinit:
	lda #$00
        sta COLUBK
	ldx #logo_datalines_end-logo_datalines-4
	ldy #logo_datalines_end-logo_datalines-1
-	lda logo_datalines,y
	sta logo_gfx_stack+3,x
	dey
	lda logo_datalines,y
	sta logo_gfx_stack+2,x
	dey
	lda logo_datalines,y
	sta logo_gfx_stack+1,x
	dey
	lda logo_datalines,y
	sta logo_gfx_stack+0,x
	txa
	sbx #$04
	dey
	bpl -

	lda #$00
	sta PF0
	sta PF1
	sta PF2
	lda #$13
	sta CTRLPF
	tsx
	stx logo_stackmerk
	ldx #$00
	lda #$2f
	jsr PosObject
	ldx #$01
	lda #$5f
	jsr PosObject
        ldx #$02
        lda #$2e
        jsr PosObject
        ldx #$03
        lda #$4f
        jsr PosObject
        ldx #$04
        lda #$68
        jsr PosObject
	sta WSYNC
	sta HMOVE
	sta WSYNC
	sta HMCLR
	lda #$ff
	sta COLUP0
	sta COLUP1
	lda #$02
	sta NUSIZ0
        sta NUSIZ1
        lda #$48
        sta COLUPF
	jsr WaitForVblankEnd
	ldx #$10
-	sta WSYNC
	dex
	bne -
	ldy #$00
	sta WSYNC
	ldx #14
-	dex
	bne -
	!byte $2c
-
	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
	ldx logo_indizes,y
	txs
	pla
	sta PF1
	pla
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	pla
	sta PF2
	pla
	sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #logo_indizes_end1-logo_indizes ; l_registers1_end-l_registers1
	bne -

-	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
        nop
        nop
        nop
        nop
	nop
	lda #$03
	sta PF1
	lda #$7c
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta+2 $00,x
	lda #$06
	nop
	sta PF2
	lda #$0c
	nop
	sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	nop
	bit $ea
	;cpy #logo_indizes_end2-logo_indizes ; l_registers1_end-l_registers1
	;bne -

-	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
        nop
        nop
        nop
	nop
	nop
	lda #$03
	sta PF1
	lda #$cc
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	lda #$03
	nop
	sta PF2
	lda #$0c
	nop
	sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #logo_indizes_end3-logo_indizes ; l_registers1_end-l_registers1
	bne -
	sta $00,x
	nop
	cmp (ram_start,x)
	jmp +
	

-	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
+	nop
	nop
        nop
        nop
        nop
	lda #$03
	sta PF1
	lda #$cc
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	lda #$3e
	nop
	sta PF2
	lda #$04
	nop
	sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #logo_indizes_end4-logo_indizes ; l_registers1_end-l_registers1
	bne -
	sta $00,x
	nop
	cmp (ram_start,x)
	jmp +

-	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
+	nop
	nop
	nop
	nop
	nop
	lda #$03
	sta PF1
	lda #$cc
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	lda #$3c
	nop
	sta PF2
	lda #$00
	nop
	sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #l_registers1_end-l_registers1
	bne -
	sta $00,x        
        !ifdef enable_scroll {
	ldy logo_stackmerk
        } else {
	ldx logo_stackmerk
	txs
        }
	lda #$00
	sta GRP0
	sta GRP1
        sta ENAM0
        sta ENAM1
        sta ENABL
        sta PF0
        sta PF1
        sta PF2
	sta COLUPF
	sta COLUP0
	sta COLUP1
        !ifdef enable_scroll {
        ldx #dynamic_vars-1
        txs
        ldx #<scroll
        lda #>scroll
        jmp jsr_far
        } else {        
	rts
        }

	+align256
	!src "logo/picture.asm"


	+align256
l_registers1:
	+logo_poke L_NOP, 0, GRP0, $c0
	+logo_poke GRP1, $03, GRP0, $e0
	+logo_poke GRP1, $07, GRP0, $f0
	+logo_poke GRP1, $0f, GRP0, $08
	+logo_poke GRP1, $10, ENABL, $02
	+logo_poke ENAM0, $02, GRP0, $0c
	+logo_poke GRP1, $30, HMM0, $f0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, GRP0, $0e
	+logo_poke GRP1, $70, L_NOP, 0
	+logo_poke NUSIZ0, $12, L_NOP, 0
	+logo_poke HMP0, $80, GRP0, $00
	+logo_poke GRP1, 0,  HMOVE, 0
        +logo_poke CTRLPF, $03, L_NOP, 0 
        +logo_poke COLUPF, $4a, L_NOP, 0 
        +logo_poke COLUPF, $48, HMOVE, 0
	+logo_poke ENABL, 0, HMCLR, 0
	+logo_poke NUSIZ1, $10, NUSIZ0, $10
	+logo_poke HMP0, $80, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke HMP0, 0, HMOVE, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke COLUPF, $2e, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke ENABL, 2, L_NOP, 0
	+logo_poke GRP0, $0e, ENAM1, 2
	+logo_poke HMM1, $10, CTRLPF, $13 
	+logo_poke COLUPF, $4e, HMBL, $c0
	+logo_poke COLUPF, $2e, HMOVE, 0
	+logo_poke L_NOP, 0, ENAM1, 0
	+logo_poke ENABL, 0, GRP1, $70
	+logo_poke GRP0, $08, NUSIZ0, $12
	+logo_poke GRP0, $c0, HMCLR, 0
	+logo_poke GRP0, $f0, GRP1, $30
 	+logo_poke GRP0, $f8, GRP1, $10
 	+logo_poke GRP0, $fc, GRP1, $07
 	+logo_poke GRP0, $fe, L_NOP, 0
	+logo_poke L_NOP, 0, GRP1, $00
 	+logo_poke GRP0, $ff, NUSIZ1, $12
	+logo_poke HMM1, $f0, ENAM1, 2
	+logo_poke L_NOP, 0, HMOVE, 0
 	+logo_poke GRP0, $00, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke ENAM1, 0, HMM1, $80
	+logo_poke NUSIZ1, $10, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, HMM1, 0
	+logo_poke HMP1, $80, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke HMM1, $10, L_NOP, 0
	+logo_poke HMP1, 0, L_NOP, 0
	+logo_poke ENAM1, 2, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke GRP1, $fe, NUSIZ1, $11
	+logo_poke ENAM1, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke GRP1, $fc, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke GRP1, $f8, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke GRP1, $f0, L_NOP, 0
	+logo_poke GRP1, $e0, L_NOP, 0
	+logo_poke GRP1, $c0, L_NOP, 0
	+logo_poke GRP1, $00, L_NOP, 0
l_registers1_end:                

l_data1 = l_registers1+$80
l_registers2 = l_registers1+$100
l_data2 = l_registers1+$180

