#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>



typedef struct
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char alpha;
} RGB;

RGB dtv_rgb[256];


#define ABS(x) ((x) < 0 ? -(x) : (x))

static unsigned long atari_rgb[128] = {
  0x000000, 0x2b2b2b, 0x525252, 0x767676,
  0x979797, 0xb6b6b6, 0xd2d2d2, 0xececec,
  0x000000, 0x2b2b2b, 0x525252, 0x767676,
  0x979797, 0xb6b6b6, 0xd2d2d2, 0xececec,
  0x805800, 0x96711a, 0xab8732, 0xbe9c48,
  0xcfaf5c, 0xdfc06f, 0xeed180, 0xfce090,
  0x445c00, 0x5e791a, 0x769332, 0x8cac48,
  0xa0c25c, 0xb3d76f, 0xc4ea80, 0xd4fc90,
  0x703400, 0x89511a, 0xa06b32, 0xb68448,
  0xc99a5c, 0xdcaf6f, 0xecc280, 0xfcd490,
  0x006414, 0x1a8035, 0x329852, 0x48b06e,
  0x5cc587, 0x6fd99e, 0x80ebb4, 0x90fcc8,
  0x700014, 0x891a35, 0xa03252, 0xb6486e,
  0xc95c87, 0xdc6f9e, 0xec80b4, 0xfc90c8,
  0x005c5c, 0x1a7676, 0x328e8e, 0x48a4a4,
  0x5cb8b8, 0x6fcbcb, 0x80dcdc, 0x90ecec,
  0x70005c, 0x841a74, 0x963289, 0xa8489e,
  0xb75cb0, 0xc66fc1, 0xd380d1, 0xe090e0,
  0x003c70, 0x195a89, 0x2f75a0, 0x448eb6,
  0x57a5c9, 0x68badc, 0x79ceec, 0x88e0fc,
  0x580070, 0x6e1a89, 0x8332a0, 0x9648b6,
  0xa75cc9, 0xb76fdc, 0xc680ec, 0xd490fc,
  0x002070, 0x193f89, 0x2f5aa0, 0x4474b6,
  0x578bc9, 0x68a1dc, 0x79b5ec, 0x88c8fc,
  0x340080, 0x4a1a96, 0x5f32ab, 0x7248be,
  0x835ccf, 0x936fdf, 0xa280ee, 0xb090fc,
  0x000088, 0x1a1a9d, 0x3232b0, 0x4848c2,
  0x5c5cd2, 0x6f6fe1, 0x8080ef, 0x9090fc,
  0x000000, 0x2b2b2b, 0x525252, 0x767676,
  0x979797, 0xb6b6b6, 0xd2d2d2, 0xececec,
  0x000000, 0x2b2b2b, 0x525252, 0x767676,
  0x979797, 0xb6b6b6, 0xd2d2d2, 0xececec
};

static void calc_dtv_rgb(void)
{
	int i;
	unsigned int r,g,b;
	memset(dtv_rgb, 0, sizeof(dtv_rgb));
	for(i=0; i < 128; ++i)
	{
		r = (atari_rgb[i] & 0xff0000) >> 16;
		g = (atari_rgb[i] & 0xff00) >> 8;
		b = (atari_rgb[i] & 0xff);
		dtv_rgb[i*2].r = r;
		dtv_rgb[i*2+1].r = r;
		dtv_rgb[i*2].g = g;
		dtv_rgb[i*2+1].g = g;
		dtv_rgb[i*2].b = b;
		dtv_rgb[i*2+1].b = b;
	}
}

static unsigned long long calc_distance(RGB e1, RGB e2)
{
  long r,g,b;
  long rmean;

  rmean = ( (int)e1.r + (int)e2.r ) / 2;
  r = (int)e1.r - (int)e2.r;
  g = (int)e1.g - (int)e2.g;
  b = (int)e1.b - (int)e2.b;
  return (((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8);
}


int get_dtv_index(RGB rgb)
{
    int i;
    int ret;
    unsigned long long min = 0xffffffffffffffffULL;
    unsigned long long cur;
    ret = 0;
    for(i=0; i < 256; ++i)
    {
        cur = calc_distance(rgb, dtv_rgb[i]);
        if(cur < min)
        {
            min = cur;
            ret = i;
        }
    }
    return ret;
}

static SDL_Surface *display;

void open_display(int x, int y)
{

  display = SDL_SetVideoMode( x, y, 16, SDL_SWSURFACE );
  if ( display == NULL )
  {
    fprintf(stderr, "Konnte kein Fenster oeffnen: %s\n",
      SDL_GetError());
    exit(1);
  }
}


int get_dtv_pixel(const SDL_Surface *image, int x, int y)
{
    int pixel_size;
    int bytes_per_line;
    int index;
    const unsigned char *pixels;
    RGB rgbval;
    
    rgbval.alpha = 0;
    if((x < 0) || (x >= image->w))
    {
        fprintf(stderr, "wrong x ccoord %d\n", x);
        exit(-1);
    } 
    if((y < 0) || (y >= image->h))
    {
        fprintf(stderr, "wrong x ccoord %d\n", x);
        exit(-1);
    } 
    pixel_size = image->format->BytesPerPixel;
    bytes_per_line = image->pitch;
    pixels = (const unsigned char *)image->pixels;
   
    switch(pixel_size)
    {
        case 1:
            index = pixels[(bytes_per_line *y)+x];
            rgbval.r = image->format->palette->colors[index].r; 
            rgbval.g = image->format->palette->colors[index].g; 
            rgbval.b = image->format->palette->colors[index].b;
            break; 
        case 3:
            rgbval.r = pixels[((bytes_per_line *y)+x*3)];
            rgbval.g = pixels[((bytes_per_line *y)+x*3)+1];
            rgbval.b = pixels[((bytes_per_line *y)+x*3)+2];
        break;
        default:
            fprintf(stderr, "not supported size of pixel : %d\n", pixel_size);
            exit(-1);
        break;
    }
    return get_dtv_index(rgbval);
    
}

unsigned char *convert_image(const SDL_Surface *image)
{
    int i,j;
    unsigned char *data;
    unsigned char *p;
    unsigned char biggest_p;
    int biggest_p_flag;
    data = (unsigned char *)malloc(image->w * image->h);
    p = data;

    for(j=image->h-1; j >= 0 ; --j)
    {
	biggest_p = 0;
	printf("	+sprdoubleline %%");
        for(i=24; i < 24+32; i+=2)
        {
             biggest_p = get_dtv_pixel(image, i, j);
	     printf("%c", biggest_p ? '#' : '.');
        }
	printf("\n");
    }
    return data; 
}

void wait_for_exit(void)
{
    SDL_Event event;
    int quit = 0;
    while(quit == 0)
    {
        SDL_Delay(100);
        while( SDL_PollEvent( &event ) )
        {
            switch( event.type )
            {
                case SDL_KEYDOWN:
                    //printf( "Press: " );
                    //printf( " Name: %s\n", SDL_GetKeyName( event.key.keysym.sym) );
                    if(strcmp(SDL_GetKeyName( event.key.keysym.sym), "q") == 0)
                    {
                        quit = 1;
                    }
                    break;

                case SDL_KEYUP:
                    //printf( "Release: " );
                    //printf( " Name: %s\n", SDL_GetKeyName( event.key.keysym.sym) );
                    break;

                case SDL_QUIT:  // SDL_QUIT  int ein schliessen des windows
                    quit = 1;
                    break;

                default:
                break;
            }
        }
    }
}


void DrawPixel(SDL_Surface *screen, int x, int y,Uint8 R, Uint8 G,Uint8 B)
{
    Uint32 color = SDL_MapRGB(screen->format, R, G, B);

    if ( SDL_MUSTLOCK(screen) )
    {
        if ( SDL_LockSurface(screen) < 0 ) {
            return;
        }
    }

    switch (screen->format->BytesPerPixel) {
        case 1: { /* vermutlich 8 Bit */
            Uint8 *bufp;

            bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
            *bufp = color;
        }
        break;

        case 2: { /* vermutlich 15 Bit oder 16 Bit */
            Uint16 *bufp;

            bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
            *bufp = color;
        }
        break;

        case 3: { /* langsamer 24-Bit-Modus, selten verwendet */
            Uint8 *bufp;

            bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
            if(SDL_BYTEORDER == SDL_LIL_ENDIAN) {
                bufp[0] = color;
                bufp[1] = color >> 8;
                bufp[2] = color >> 16;
            } else {
                bufp[2] = color;
                bufp[1] = color >> 8;
                bufp[0] = color >> 16;
            }
        }
        break;

        case 4: { /* vermutlich 32 Bit */
            Uint32 *bufp;

            bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
            *bufp = color;
        }
        break;
    }

    if ( SDL_MUSTLOCK(screen) )
    {
        SDL_UnlockSurface(screen);
    }
}

void make_converted_image(const unsigned char *data, SDL_Surface *image)
{
    const unsigned char *p;
    p = data;
    int i,j;
    for(j=0; j < image->h; ++j)
    {
        for(i=0; i < image->w; ++i)
        {
            DrawPixel(image, i, j, dtv_rgb[(int)(*p)].r, dtv_rgb[(int)(*p)].g, dtv_rgb[(int)(*p)].b);
            ++p;
        }
    }
}

unsigned char datalines[512][5];
unsigned char dataline_indizes[512];
int current_dataline = 0;
int current_index = 0;

void add_to_dataline_pool(unsigned char *dline)
{
	int i;
	for(i=0; i < current_dataline; ++i)
	{
		if (memcmp(datalines[i], dline, 5) == 0)
		{
			break;
		}
	}
	if (i >= current_dataline)
	{
		memcpy(datalines[i], dline, 5);
		++current_dataline;
	}
	dataline_indizes[current_index++] = i;
}

unsigned char switchbits(unsigned char a)
{
	int i;
	unsigned char b = 0;
	b = 0;
	for(i=0; i < 8; ++i)
	{
		if(a & (128 >> i))
			b |= 1 << i;
	}
	return b;
}


void show_image(const char *name, FILE *f)
{
    SDL_Surface *image;
    SDL_Surface *converted_image;
    SDL_Rect srect, drect;
    int i,j,k;
    
    unsigned char *data;
    unsigned char d;
    unsigned char dataline[5];
    unsigned char *datalinep;


    image = IMG_Load(name);
    converted_image = IMG_Load(name);

    if (image == NULL)
    {
        fprintf(stderr, "Das Bild konnte nicht geladen werden:%s\n",
        SDL_GetError());
        exit(-1);
    }

    //printf("Size of Image : %d x %d\n", image->w, image->h); 
    open_display(image->w*2, image->h);
    // kopiere das Bild-Surface auf das display-surface
    SDL_BlitSurface(image, NULL, display, NULL);

    // den veraenderten Bereich des display-surface auffrischen
    data = convert_image(image);
    //fwrite(memory, sizeof(memory), 1, f);
    make_converted_image(data, converted_image);
    srect.x = 0;
    srect.y = 0;
    srect.w = image->w;  // das gesamte Bild
    srect.h = image->h;   // das gesamte Bild

    // Setzen des Zielbereichs
    drect.x = image->w;
    drect.y = 0;
    drect.w = image->w;
    drect.h = image->h;


    SDL_BlitSurface(converted_image, &srect, display, &drect);
    SDL_Flip(display);
    wait_for_exit();
    free(data);
    //fclose(f);
    
    SDL_FreeSurface(image);
}


int main(int argc, char **argv)
{
    calc_dtv_rgb();

    FILE *f;
    atexit(SDL_Quit);
    if(argc < 3)
    {
        printf("usage : %s <in_file> <out_file>\n", argv[0]);
        exit(-1);
    }
    //f = fopen(argv[2], "wb");
    if (!f)
    {
        fprintf(stderr, "cannot open \"%s\"\n", argv[1]);
        exit(-1);
    }
    if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
       fprintf(stderr, "SDL konnte nicht initialisiert werden:  %s\n",
         SDL_GetError());
       exit(1);
    }
    show_image(argv[1], f);
    return 0;
}
