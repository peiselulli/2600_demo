sinbar_y_pos = ram_start
sinbar_x_pos = ram_start+1
ptr1 = ram_start+2
ptr2 = ram_start+4
sinbar_code = ram_start+6

sinbar:
	lda initflag
        bne sinbar_noinit
	lda #$40
	sta part_duration
	;lda #$00
	;sta COLUBK
        ;sta PF0
        ;sta PF1
        ;sta PF2
        ldx #sinbar_codelen-1
-	lda sinbar_code_copy,x
	sta sinbar_code,x
        dex
        bpl -
        lda #$00
        sta sinbar_x_pos
        
sinbar_noinit:
        lda sinbar_y_pos
        asr #$3f*2
        tax
        lda sintab1,x
        sta ptr1
        sta ptr2
        lda #>charsetleft
        sta ptr1+1
        lda #>charsetright
        sta ptr2+1
        inc sinbar_y_pos
        ldx sinbar_x_pos
        lda sintab2,x
        sta tmp1
	inx
        cpx #sintab2_len
        bne +
        ldx #$00
+	stx sinbar_x_pos        

	inc <sbx1
	inc <sbx1
	dec <sbx2
	dec <sbx2
	inc <sbx3
	inc <sbx3
	dec <sbx4
	dec <sbx4
	inc <sbx5
	inc <sbx5
	dec <sbx6
	dec <sbx6
	inc <sbx7
	inc <sbx7
	dec <sbx8
	dec <sbx8
	inc <sbx9
	inc <sbx9
	dec <sbxa
	dec <sbxa
        
        lda #$20
        sta NUSIZ0
        sta NUSIZ1
        lda tmp1
        ldx #$00
        jsr PosObject
        lda tmp1
        clc
        adc #$08
        ldx #$01
        jsr PosObject
        lda tmp1
        sec
        sbc #3
        ldx #$02
        jsr PosObject
        lda tmp1
	clc
        adc #8+8+1
        ldx #$03
        jsr PosObject
	STA WSYNC
	STA HMOVE
	jsr WaitForVblankEnd
        lda #$ff
        sta GRP0
        sta GRP1
        sta ENAM0
        sta ENAM1
        lda #$b0
        sta COLUP0
        sta COLUP1 
        
        lda sinbar_y_pos
        asr #$3f*2
        tax
        lda sintab1,x
	ldx sinbar_x_pos
        adc sintab2,x
        lsr
        sta <(sinbar_code+1)
        jmp sinbar_code

sinbar_code_copy:
	!pseudopc sinbar_code {
	ldx #$00
        ldy #227
        bit $ea
	sta WSYNC
-	sax COLUBK
        lda (ptr1),y
        sta GRP0
        lda (ptr2),y
        sta GRP1
        lda #$3f
sbx1 = *+1
        sbx #$20
	sax COLUBK
sbx2 = *+1
        sbx #$20
	sax COLUBK
sbx3 = *+1
        sbx #$20
	sax COLUBK
sbx4 = *+1
        sbx #$10
	sax COLUBK
sbx5 = *+1
        sbx #$20
	sax COLUBK
sbx6 = *+1
        sbx #$10
	sax COLUBK
sbx7 = *+1
        sbx #$10
	sax COLUBK
sbx8 = *+1
        sbx #$20
	sax COLUBK
sbx9 = *+1
        sbx #$20
	sax COLUBK
sbxa = *+1
        sbx #$0f
	sax COLUBK
        dey
        bne -
        sty COLUBK
        rts
        }

sinbar_codelen = * - sinbar_code_copy         

sintab1:
	!bin "sinbar/sintab1"
sintab2:
	!bin "sinbar/sintab2"
sintab2_len = * - sintab2
       
	+align256
charsetleft:
	!byte 0
        
	!ifdef commented_out {
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %#######..#######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %######....######
        +sprdoubleline %#######..#######
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %####........####
        +sprdoubleline %##............##
        +sprdoubleline %#..............#
        +sprdoubleline %.......##.......
        +sprdoubleline %......####......
        +sprdoubleline %.....######.....
        +sprdoubleline %....########....
        +sprdoubleline %#...########....
        +sprdoubleline %############....
        +sprdoubleline %############....
        +sprdoubleline %############....
        +sprdoubleline %############....
        +sprdoubleline %############....
        +sprdoubleline %###########.....
        +sprdoubleline %##########.....#
        +sprdoubleline %#########....###
        +sprdoubleline %########....####
        +sprdoubleline %######....######
        +sprdoubleline %#####....#######
        +sprdoubleline %###....#########
        +sprdoubleline %#.....##########
        +sprdoubleline %....############
        +sprdoubleline %....############
        +sprdoubleline %....############
        +sprdoubleline %....############
        +sprdoubleline %....############
        +sprdoubleline %....########...#
        +sprdoubleline %....########....
        +sprdoubleline %.....######.....
        +sprdoubleline %......####......
        +sprdoubleline %.......##.......
        +sprdoubleline %#.............##
        +sprdoubleline %##............##
        +sprdoubleline %####........####
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %##.###########.#
        +sprdoubleline %#...#########...
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...#######...##
        +sprdoubleline %#...#######...##
        +sprdoubleline %#...#######...##
        +sprdoubleline %#...######...###
        +sprdoubleline %#...######...###
        +sprdoubleline %#...######...###
        +sprdoubleline %#...#####...####
        +sprdoubleline %#...#####...####
        +sprdoubleline %#...#####...####
        +sprdoubleline %#...####...#####
        +sprdoubleline %#...####...#####
        +sprdoubleline %#...###...######
        +sprdoubleline %#....##...######
        +sprdoubleline %#.........######
        +sprdoubleline %#.........######
        +sprdoubleline %#..........#####
        +sprdoubleline %#............###
        +sprdoubleline %#.............##
        +sprdoubleline %#......##......#
        +sprdoubleline %#.....####.....#
        +sprdoubleline %#....######....#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#...########...#
        +sprdoubleline %#....######....#
        +sprdoubleline %#.....####.....#
        +sprdoubleline %#..............#
        +sprdoubleline %#..............#
        +sprdoubleline %#..............#
        +sprdoubleline %#............###
        +sprdoubleline %##........######
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %#######..#######
        +sprdoubleline %######....######
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#####......#####
        +sprdoubleline %#..............#
        +sprdoubleline %................
        +sprdoubleline %................
        +sprdoubleline %................
        +sprdoubleline %................
        +sprdoubleline %#..............#
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        +sprdoubleline %################
        } else {
	!src "sinbar/sprdata.asm"
	}        
charsetright = charsetleft+$100

	* = charsetright+$100
        
