#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int i;
        int v;
        double a;
        FILE *f;
        
        f = fopen("sintab1", "wb");
	for (i=0; i < 64; ++i)
        {
        	a = sin((double)i / 32.0 * M_PI) * ((255.0-227.0)/2.0);
                a += ((256.0-227.0)/2.0);
                v = (int)a;
                fputc(v, f);
        }
        fclose(f);
        f = fopen("sintab2", "wb");
        for (i=0; i < 160; ++i)
        {
        	a = sin((double)i / 80.0 * M_PI) * ((0x88-0x14)/2.0);
                a += ((0x88-0x14)/2.0)+0x14;
                v = (int)a;
                printf("%02x\n", v);
                fputc(v, f);
        	
        }
        fclose(f);
}
