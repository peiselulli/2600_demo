	sm_textpos = ram_start
        sm_pos_in_char = ram_start+2
	sm_chars = ram_start+3

sprite_multiplier:
	lda initflag
        bne sprite_multiplier_no_init
        lda #$A0
        sta part_duration
        lda #$00
        sta sm_pos_in_char
        lda #<sm_text
        sta sm_textpos
        lda #>sm_text
        sta sm_textpos+1
        ldx #110
        lda #$00
-       sta sm_chars,x
        dex
        bne -
	lda #$34
	sta CTRLPF
	ldx #$04
	lda #$a6
	jsr PosObject
	lda #$02
	sta ENABL
	sta WSYNC
	sta HMOVE
        jmp WaitForVblankEnd
        
sprite_multiplier_no_init:        
        lda #$03
        sta NUSIZ0
        sta NUSIZ1
        ldx #108-1
-	
	lda sm_chars,x
	sta sm_chars+1,x
        dex
        bne -
        lda #$00
        sta sm_chars+108
        ldy #$00
        lda (sm_textpos),y
        asl
        asl
        asl
	sta tmp2
	lda sm_pos_in_char
	and #$07
        ora tmp2
        tax
        lda sm_charset,x
        ;sta sm_chars+2
        ;lda sm_charset+1,x
	sta sm_chars+1       
        lda sm_pos_in_char
        clc
        adc #$01
        sta sm_pos_in_char
        and #$07
        bne +
        inc sm_textpos
        bne +
        inc sm_textpos+1
+	jsr WaitForVblankEnd
	lda #$00
	sta WSYNC
	sta COLUBK
	lda sm_pos_in_char
	lsr
	sta tmp1
	jmp sm_continue

	+align256
sm_continue:        
        ldx #108
        sta WSYNC
-
        txa
        clc
        adc tmp1
        and #$7f
        eor #$7f
        tay
        lda sm_chars,x
        sta GRP0
        sta GRP1
	sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
	sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
	sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
        sta RESP0
	sta RESP1
        nop
        bit $ea

	bit $ea
        lda sm_colors,x
        sta COLUP0
        lda sm_colors,y
        sta COLUP1
        bit GRP0
        bit GRP1
	sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
	sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
	sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
        sta RESP0
        sta RESP1
        sta RESP0
	sta RESP1
	dex
        bne -

        sax GRP0
        sax GRP1
        sax COLUP0
        sax COLUP1
	sta WSYNC
	sta WSYNC                
	sta WSYNC                
	sax COLUBK
	rts

sm_text:
	!ct "sprite_multiplier/topet.ct" {
        !text "   greetings to  ... "
			;!text "accession "          ; 9 =6
			;!text "alcatraz "           ; 8 =8
			!text "arsenic "                    ; 7 =9
			;!text "atebit "                     ; 6 =11
			;!text "bauknecht "          ; 9 =6
			;!text "bitfellas "          ; 9 =6
			;!text "booze designs "              ; 13 =0
			;!text "brainstorm "          ; 10 =5
			;!text "chorus "                     ; 6 =11
			!text "censor design "         ; 10 =5
			;!text "conspiracy "         ; 10 =5
			;!text "crest "                      ; 5 =12
			;!text "dekadence "          ; 9 =6
			;!text "division zero "              ;
			;!text "drifters "           ; 8 =8
			;!text "dss "                        ; 3 =15
			;!text "fairlight "          ; 9 =6
			;!text "farbrausch "         ; 10 =5
			;!text "focus "                      ; 5 =12
			!text "hitmen "                     ; 6 =11
                        !text "hochniveau "
			!text "metalvotze "         ; 10 =5
			!text "nuance "                     ; 6 =11
			!text "onslaught "          ; 9 =6
			;!text "orb "                        ; 3 =15
			!text "oxyron "                     ; 6 =11
			!text "plush "
			!text "rabenauge "          ; 9 =6
			;!text "rbbs "                       ; 4 =14
			;!text "resource "           ; 8 =8
			!text "rgcd "           ; 8 =8
			;!text "sidwave "                    ; 7 =9
			!text "speckdrumm "         ; 10 =5
			;!text "still "                      ; 5 =12
			!text "the dreams "         ; 10 =5
			;!text "triad "                      ; 5 =12
			;!text "uprough "                    ; 7 =9
			;!text "vision "                     ; 6 =11
                        !text "                         "
        }


	+align256
sm_charset:
	!bin "sprite_multiplier/charset"
	
	+align256
sm_colors:
	!src "sprite_multiplier/colors.asm"
        

