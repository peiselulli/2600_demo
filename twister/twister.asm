        twister_phase = ram_start+1
        tw_y = ram_start+2
        TWISTER_RAM = ram_start+3
        
twister:
	lda initflag
        bne tw_no_init
	lda #$80
	sta part_duration
        ldx #twisterram_len-1
-       lda twisterram_copy,x
	sta TWISTER_RAM,x
        dex
        bpl -
        lda #$00
        ;sta PF0
        ;sta PF1
        ;sta PF2
        sta tw_y        	
	;STA COLUP0
	;STA COLUP1
	;lda #$00
        ;sta ENAM0
        ;sta ENAM1        
	lda #$03
	sta NUSIZ0
	sta NUSIZ1
        lda #$01
        sta CTRLPF
        lda #$f0
        sta PF0
        lda #$ff
        sta PF1
        lda #$07
        sta PF2
        lda #$02
        ;sta ENAM0
        ;sta ENAM1
	lda #$3f
	ldx #$00
	jsr PosObject
        lda #$47
        ldx #$01
	jsr PosObject
        ;lda #$19
        ;ldx #$02
        ;jsr PosObject
        ;lda #$71
        ;ldx #$03
        ;jsr PosObject
        lda #$01
        sta VDELP0
        sta VDELP1
tw_no_init:
	ldy #$e6
	ldx twister_phase
        bpl +
        ldy #$c6
+        
        lda twister_table,x
        sta <(tr_add+1)
	sty <(tr_make_dec)
        inc twister_phase       
	lda #$00
	sta GRP0
        sta GRP1
	sta GRP0
        sta <(tr_value+1)
	sta COLUP0
        sta COLUP1
        lda #$c0
        sta COLUPF
	lda #$90
        sta tmp1
        lda #$00
        sta tmp2
        
	lda tw_y
	sta <(line_counter+1)
        dec tw_y
        dec tw_y
	STA WSYNC
	STA HMOVE
        ;tsx
        ;stx tmp1
	;jsr WaitForVblankEnd

-	STA WSYNC	
	LDA INTIM	
	BNE -
        lda #18
        sta T1024T


	ldy #$00
        lda (line_counter+1),y
        tay
        lda #$40
        sta COLUBK
        ldx #$00
        lda gfxdata,y
	STA WSYNC
	stx+2 VBLANK
        jmp TWISTER_RAM+3
tr_no_dec:
	lda #$02
        sta VBLANK        
        lda #$00
        sta GRP0
        sta GRP1
        sta GRP0
        sta COLUPF
	LDA #2		
	STA WSYNC  	
	STA VBLANK
        lda #41
        sta TIM64T
        ;ldx tmp1
        ;txs
	+skip_framewait
	rts	

twisterram_copy:
        !pseudopc TWISTER_RAM {
twist_loop:
        lda gfxdata,y
        sta GRP0
        lda gfxdata+$20,y
        sta GRP1
        lda gfxdata+$40,y
        sta GRP0
        lda gfxdata+$60,y
        sta <(twister_data+1)
        ldx gfxdata+$80,y
        lda gfxdata+$a0,y
twister_data:
	ldy #$00
        sty GRP1
        stx GRP0
        sta GRP1
        sta GRP0
line_counter:
        ldy move_tab
tr_value:
        lda #$00
tr_add:	adc #$00 
        sta <(tr_value+1)
        bcs tr_make_dec
	sty COLUP0
        sty COLUP1
        lda INTIM
	bne twist_loop
	jmp tr_no_dec
                       
tr_make_dec:
	dec <(line_counter+1)
        lda INTIM
	bne twist_loop
        jmp tr_no_dec
        }
twisterram_len = * - twisterram_copy

	+align256
gfxdata:
	!for j, $20 {
        !byte 0
        }
        !for j, 6 {
	!for i,16 {
      	!bin "twister/twistdat",1,(32-i)*8+(j-1)
        }
	!for i,16 {
      	!bin "twister/twistdat",1,(i-1)*8+(j-1)
        }
        } 

	+align256        
move_tab:
	!for j, 8 {
	!for i, 16 {
	!byte (i-1)+$20 
        }
        !for i, 16 {
        !byte 32-i+$20
        }
        }
        
twister_table:
        !for i, 64 {
        !byte (i-1)*2
        }
	!for i, 64 {
        !byte (64-i)*2
        }
        !for i, 64 {
        !byte (i-1)*2
        }
	!for i, 64 {
        !byte (64-i)*2
        }
        
         
            
