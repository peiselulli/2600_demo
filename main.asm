	RUNTIME_MAX = $c0

!src "config.asm"
!src "include/globalvars.asm"

	!cpu 6510
	!src "include/vcs.h"
	!src "include/macros.h"
        * = $1000
        
        !bin "basics.bin"
        !src "basics.lab"
        
Start:
	SEI	
	CLD
        ldx #0 
        txa 
Clear:  dex 
        txs 
        pha 
        bne Clear
	sta SWACNT
newMainLoop:
	+jmp MainLoop

        !src "song/song.asm"


continue_songplay:
        !src "song/songplay.asm"
OverScanWait:
	STA WSYNC
	LDA INTIM	
	BNE OverScanWait
	JMP  newMainLoop      


	!ifdef enable_waves {
!src "waves/waves.asm"
	}                
                 
	+make_vectors
        
        * = $3000
        !bin "basics.bin"       
	!ifdef enable_sinbar {
!src "sinbar/sinbar.asm"
	}
	!ifdef enable_vector {
!src "vector/vector.asm"
	}
	!ifdef enable_twister {
!src "twister/twister.asm"
	}

MainLoop:
	LDA  #2
	STA  VSYNC	
	STA  WSYNC	
	STA  WSYNC 	
	STA  WSYNC	
	LDA  #53	
	STA  TIM64T	
	LDA #0		
	STA  VSYNC
	sta HMCLR
        ldy current_part
        lda #>(FrameWait-1)
        pha
        lda #<(FrameWait-1)
        pha
        lda #>(framewait_cont-1)
        pha
        lda #<(framewait_cont-1)
        pha
        ldx jumptab,Y
        lda jumptab+1,Y
        jmp jsr_far


FrameWait:
	LDA INTIM	
	BNE FrameWait

	LDA #2		
	STA WSYNC  	
	STA VBLANK
        lda #42
        sta TIM64T
FrameWait_Cont:
	ldx #$f0        	
        inc initflag
	lda initflag
	and #$07
        bne continue_with_overscan_no_stx
        dec part_duration
        bne continue_with_overscan
+	lax current_part
        sbx #$fe
;        lda jumptab+1,x
;        bne +
;        ldx #$00
+	stx current_part       

	;clear tia
	ldx #$2c-4-1
        lda #$00
-	sta $04,x
	dex
        bpl -
	inx

continue_with_overscan:	
	stx initflag
continue_with_overscan_no_stx:
	!ifdef enable_startpic {
	lda current_part
        bne +
        +jmp OverScanWait
        } 
+	+jmp continue_songplay


jumptab:        
	!ifdef enable_startpic {
	!word startpic
	}
	!ifdef enable_sinbar {
	!word sinbar
	}
	!ifdef enable_vector {
	!word vector
	}
	!ifdef enable_waves {
	!word waves
	}
	!ifdef enable_twister {
	!word twister
	}
	!ifdef enable_ball {
	!word ball
	}
	!ifdef enable_crystal {
	!word crystal
	}
	!ifdef enable_checker {
	!word checker
	}
	!ifdef enable_checker2 {
	!word checker2
	}
	!ifdef enable_scroll {
	!ifdef enable_logo {
	} else {
	!word scroll
	}
	}
	!ifdef enable_sprite_multiplier {
        !word sprite_multiplier
	} 
	!ifdef enable_armelyte {
	!word armelyte
	}       
	!ifdef enable_tuffscroll {
	!word tuffscroll
	}
	!ifdef enable_logo {
	!word logo
	}
        !word jump_to_start

	!ifdef enable_waves {
!src "waves/waves2.asm"
        }

        +make_vectors


        * = $5000
        !bin "basics.bin"        

        !ifdef enable_ball {
!src "ball/ball.asm"
	}

        +make_vectors

        * = $7000
        !bin "basics.bin"
	!src "include/helpers.asm"
	!ifdef enable_crystal {
!src "crystal/crystal.asm"
	}        
	!ifdef enable_logo {
!src "logo/logo.asm"
	}

        +make_vectors

	* = $9000
        !bin "basics.bin"
        !ifdef enable_checker {
        !set check_enable = 1
        }
        !ifdef enable_checker2 {
        !set check_enable = 1
        }
	!ifdef check_enable {
!src "checker/checker.asm"
	}
        +make_vectors

	* = $b000
        !bin "basics.bin"
	!ifdef enable_tuffscroll {
!src "tuffscroll/tuffscroll.asm"
	}
	!ifdef enable_scroll {
!src "scroll/scroll.asm"
	}
	!ifdef enable_sprite_multiplier {
!src "sprite_multiplier/sprite_multiplier.asm"
	}
        !ifdef enable_startpic {
!src "startpic/startpic.asm"
	}       
        +make_vectors

	* = $d000
        !bin "basics.bin"
        !src "include/helpers.asm"
	!ifdef enable_armelyte {
!src "armelyte/armelyte.asm"
	}
        +make_vectors

	* = $f000
        !bin "basics.bin"
        !src "include/helpers.asm"
        
	!ifdef enable_armelyte {
armelyte_3:
	!src "armelyte/code3.asm"
armelyte_4:
	!src "armelyte/code4.asm"
armelyte_5:
	!src "armelyte/code5.asm"
armelyte_6:
	!src "armelyte/code6.asm"
	}
        +make_vectors
	

