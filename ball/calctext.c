#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char charset[0x400];

unsigned char text[] = "  here you get it ... a big ball for a little machine !";

void print_bin(FILE *f, int val)
{
	int i;
        fprintf(f, "   !byte %%");
        for(i=7; i >= 0; --i)
        {
        	fprintf(f, "%c", (val & 1 << i) ? '#' : '.');
        }
        fprintf(f, "\n");
}

const char *chartext = "abcdefghijklmnopqrstuvwxyz.,! ";

int map(int c)
{
	int i;
        for (i=0; i < strlen(chartext); ++i)
        {
        	if(c == chartext[i])
                	break;
        }
        if (i > strlen(chartext))
        	i = strlen(chartext)-1;
        return i;
}

int main(void)
{
	unsigned char *ch_ptr;
	unsigned char *t_ptr;
        int i;
        int c;
	FILE *f;
        f = fopen("charset", "rb");
        //fread(charset, 2, 1, f);
        fread(charset, sizeof(charset), 1, f);
        fclose(f);
        f = fopen("text.asm", "wb");
        for (t_ptr = text; *t_ptr; ++t_ptr) {
        	c = map(tolower(*t_ptr));
        	ch_ptr = charset+ ((c & 0x3f)*8);
        	for(i=0; i < 8; ++i)
                {

                	print_bin(f, ch_ptr[i]);
                }
        }
        fclose(f);
        return 0;
}
