#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL/SDL.h>

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240
#define SCREEN_DEPTH 16

SDL_Surface* screen =NULL;

SDL_Rect coord = {
	.w = 2,
        .h = 1,
};

unsigned int renderbuffer_start[SCREEN_HEIGHT];
unsigned int renderbuffer_end[SCREEN_HEIGHT];


void plot(int x, int y, int color)
{
	coord.x = x*2;
        coord.y = y;	
	SDL_FillRect(screen, &coord, color);
}

void do_plot(int x, int y)
{
	x = (int)(((double)x)*1.0);
	if(x < renderbuffer_start[y])
        	renderbuffer_start[y] = x;
        
	if(x > renderbuffer_end[y])
        	renderbuffer_end[y] = x;        
}

void plot_ball(int radius)
{
    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;
    int x0 = 160;
    int y0 = radius;
 
    memset(renderbuffer_start, SCREEN_WIDTH, sizeof(renderbuffer_start));
    memset(renderbuffer_end, 0, sizeof(renderbuffer_end));

    do_plot(x0/2, y0 + radius);
    do_plot(x0/2, y0 - radius);
    do_plot((x0 + radius)/2, y0);
    do_plot((x0 - radius)/2, y0);
 
    while(x < y)
    {
      if(f >= 0)
      {
        y--;
        ddF_y += 2;
        f += ddF_y;
      }
      x++;
      ddF_x += 2;
      f += ddF_x + 1;
 
      do_plot((x0 + x)/2, y0 + y);
      do_plot((x0 - x)/2, y0 + y);
      do_plot((x0 + x)/2, y0 - y);
      do_plot((x0 - x)/2, y0 - y);
      do_plot((x0 + y)/2, y0 + x);
      do_plot((x0 - y)/2, y0 + x);
      do_plot((x0 + y)/2, y0 - x);
      do_plot((x0 - y)/2, y0 - x);
    }
    FILE *fi;
    char name[80];
    sprintf(name, "ball%d.asm", radius);
    fi = fopen(name, "wb");
    fprintf(fi, "!byte %d ; size\n", radius);
    for(y=0; y < radius; ++y)
    {
    	if(renderbuffer_start[y] < renderbuffer_end[y])
        {
        	fprintf(fi, "!byte %d\n", renderbuffer_start[y]);
                printf("%d %d\n", renderbuffer_start[y], renderbuffer_end[y]);
        }
    	for(x = renderbuffer_start[y]; x < renderbuffer_end[y]; ++x)
        {
        	plot(x, y, 0xffff);
        }
    }
    fclose(fi);
}

int main(int argc, char *argv[]) 
{

    SDL_Event evt; 
    int done = 0; 
    SDL_Rect clear = {
        .x = 0,
        .y = 0,
	.w = SCREEN_WIDTH,
        .h = SCREEN_HEIGHT,
    };

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        exit (1);
    }

    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_DEPTH, SDL_SWSURFACE);
    if (screen == NULL)
    {
        SDL_Quit();
        exit (2);
    }

    int i = 100;
    while(!done) 
    {
	SDL_FillRect(screen, &clear, 0);
        if( SDL_PollEvent(&evt) ) 
        { 
            switch (evt.type)
            {
                        case SDL_KEYDOWN:
                             /* quit on any key press */
                             done = 1; 
                             break;
                        case SDL_QUIT:
                             done = 1; 
                             break;
            }
        }
	plot_ball(i);
        i = i -2;
        if(i < 52)
        	exit(0);

#define LOCK_FPS_TO_60FPS
#undef LOCK_FPS_TO_60FPS
#ifdef LOCK_FPS_TO_60FPS
        /* Alternative is to sleep, however sleep may not have the expected granularity */
        if ( currentTime - past >= 16 ) 
#endif /* LOCK_FPS_TO_60FPS */
        { 

            #define ALWAYS_FLIP
            #ifdef ALWAYS_FLIP
            SDL_UpdateRect(screen, 0, 0, 0, 0);
            #endif /* ALWAYS_FLIP */
        }
    }

}
