        b_radius = ram_start
	b_stack_val = tmp1
        b_value = ram_start+2
        b_yval = ram_start+3
        b_yval_low = ram_start+4
        b_zoom_direction = b_yval_low
        b_dyval = ram_start+6
        b_dyval_low = ram_start+7
        b_modus = ram_start+8
	b_color = ram_start+9
	b_color_diff = ram_start+10
        ball_zp = ram_start+11
        
	B_COLOR = $c0

ball:
	lda initflag
        bne ball_no_init
	lda #$80
	sta part_duration
	lda #B_COLOR
	sta b_color
	lda #$01
	sta b_color_diff
        ldx #ball_zp_len-1
-	lda ball_zp_copy,x
	sta ball_zp,x
        dex
        bpl - 
        lda #$00
        sta b_yval
        sta b_yval_low
        sta b_dyval
        sta b_dyval_low
        
        ;lda #$df
        ;sta <b_sub1
        ;sta <b_sub2               
        lda #$20
        sta NUSIZ1
        lda #$68
        sta COLUP1
        ;lda #$70
	lda #$6f
        sta COLUPF
        lda #$a0
        sta COLUBK
        lda #$01 ; $05
        sta CTRLPF
        lda #$00
        sta b_radius
        ldx #$02
        lda #$05
        jsr PosObject
        ldx #$00
        lda #$8f
        jsr PosObject

        ldx #$01
        lda #$9f
        jsr PosObject
        lda #$ff
        sta GRP0
        sta ENAM0
        lda #$35
        sta NUSIZ0
        lda #$00
        sta b_modus
	lda #<b_text
	sta <b_scroll_base1
	lda #>b_text
	sta <b_scroll_base1+1
	lda #$00
	sta <b_ptr
        jmp no_ball_sub

ball_no_init:
	lda <b_scroll_pointer
	lsr
	bcc ball_scroll_half
	inc <b_scroll_base1
	bne +
	inc <b_scroll_base1+1
+	lda #$00
	!byte $2c
ball_scroll_half:
	lda #$01
	sta <b_scroll_pointer
no_ball_sub:
	lda b_color
	clc
	adc b_color_diff
	sta b_color
	cmp #B_COLOR
	beq +
	cmp #B_COLOR+$f
	bne ++
+	lda b_color_diff
	eor #$ff
	clc
	adc #$01
	sta b_color_diff
++
	lda #<sprmove_neg
	sta <spr_move_ptr
	lda #<ballmove_neg
	sta <ball_move_ptr
	lda #$c8
	sta <b_iny_ptr
	lda #$24
	sta <b_jump

        +skip_framewait
	stx b_stack_val
	lda b_modus
        beq +
        jmp b_modus1
+
	lda b_dyval_low
        clc
        adc #$20
        sta b_dyval_low
        bcc +
        inc b_dyval
+	lda b_yval_low
	clc
        adc b_dyval_low
        sta b_yval_low
        lda b_yval
        adc b_dyval
        sta b_yval
        clc
        adc b_radius
        clc
        adc b_radius
        cmp #$70
        bcc +
        lda #$6f
        sec
        sbc b_radius
        sec
        sbc b_radius
        sta b_yval
        anc #$00
        sta b_yval_low
        sbc b_dyval_low
        sta b_dyval_low
        lda #$00
        sbc b_dyval
        sta b_dyval
        inc b_radius
        inc b_radius
        lda b_radius
        cmp #$10
        bne +
        inc b_modus
+	jmp end_modus

b_modus1:
	cmp #1
        bne b_modus2
	lda b_yval
        cmp #40
        beq +
        dec b_yval
        !byte $2c
+	inc b_modus
	lda #$00
        sta b_zoom_direction
	jmp end_modus        
b_modus2:
	lda b_zoom_direction
        bne b_zoom_in
        dec b_yval
        dec b_yval
        inc b_radius
        inc b_radius
        lda b_radius
        cmp #$2e
        bne end_modus
        inc b_zoom_direction        
b_zoom_in:
        inc b_yval
        inc b_yval
        dec b_radius
        dec b_radius
        lda b_radius
        cmp #$00
        bne end_modus
        dec b_zoom_direction
end_modus:
	lda #$00
        sta PF0
        sta PF1
        sta PF2
        sta GRP1
        sta ENAM1
        lda #$80
        sta b_value
+	ldx b_radius
        lda balltable,x
        sta <b_ptr
	;sta <b_ptr_copy
        lda balltable+1,x
        sta <(b_ptr+1)
	;sta <(b_ptr_copy+1)

	ldy #$00
	lda (b_ptr),y
        sta <b_last_y
        ;inc <b_last_y
        lda #75  ; ????
        sta <b_last_x
        ldx #$01
        clc
        adc #$0a
        jsr PosObject
        lda #$9d
        sec
        sbc <b_last_x
        ldx #$03
        jsr PosObject        
	sta WSYNC
        sta HMOVE
	lda #$00
	sta COLUP0

	lda <b_scroll_pointer
	lsr
	tay
	lda (b_scroll_base1),y
	sta GRP0
	;inc <b_scroll_base2+1
	jsr WaitForVblankEnd
        sta HMCLR
	lda b_color
	sta COLUP0
        ldy #$01 
        lax (b_ptr),y
	txs
        
	ldx b_yval
        beq +
-	sta WSYNC
	sta HMOVE
	lda <b_scroll_pointer
	lsr
	tay
	lda (b_scroll_base1),y
	sta GRP0
	inc <b_scroll_pointer
	dex
        bne -
+       sta WSYNC
	sta HMOVE
	lda <b_scroll_pointer
	lsr
	tay
	lda (b_scroll_base1),y
	sta GRP0
	inc <b_scroll_pointer
        ldy #$06
-	dey
	bne -
        clc
        lda #$0f
        sta GRP1
        sta ENAM1        
        ldy #$01                   
        jmp ball1half
ball1half_ret:
        sta HMCLR
	sta HMOVE
        lda #$00
        sta PF2
        sta GRP1
        sta ENAM1

-	lda <b_scroll_pointer
	lsr
	tay
	lda (b_scroll_base1),y
	sta GRP0
	inc <b_scroll_pointer
	sta WSYNC
        sta HMOVE
	lda #$00
        sta PF0
        sta PF1
        sta PF2
        sta GRP1

	lda INTIM	
	bne -

	LDA #2		
	STA WSYNC  	
	STA VBLANK
        lda #42
        sta TIM64T
	ldx b_stack_val
	txs
	rts

	;will be called from zp function
prepare_next_half:
	;sta HMCLR
        ;dey
	sty <b_store_y
	lda <b_scroll_pointer
	lsr
	tay
	lda (b_scroll_base1),y
	sta GRP0
	inc <b_scroll_pointer
	lda #<ballmove
	sta <ball_move_ptr
	lda #<sprmove
	sta <spr_move_ptr
	lda #$f0
	;lsr
	sta <b_jump
	lda #$88
	sta <b_iny_ptr
	lda #$00
	sta <b_last_y
	jmp ball1half_continue

ball_zp_copy:
	!pseudopc ball_zp {
ball1half_continue:
b_store_y = <( *+1)
	ldy #$00
ball1half:        
        sta WSYNC
        sta HMOVE
;b_ptr = <(*+1)
        tsx
        lda pf1_array,x
        sta PF1
        lda pf2_array,x
        sta PF2
b_iny_ptr:
        iny
b_ptr = <(*+1)
	lax $ffff,y
	txs
b_last_x = <(*+1)
        sbx #$00
        sta <b_last_x
spr_move_ptr = <(*+1)
        lda sprmove_neg,x
        sta HMP1
ball_move_ptr = <(*+1)
        lda ballmove_neg,x
        sta HMM1
b_scroll_pointer = < (*+1)
	lda #$00
	lsr
	tax
b_scroll_base1 = <(*+1)
	lda $f000,x
	sta GRP0
	inc <b_scroll_pointer		
b_last_y = < (*+1)
        cpy #$00
        bne ball1half
b_jump:
	beq b_doe
        sta WSYNC
	sta HMOVE
	jmp prepare_next_half
b_doe:	jmp ball1half_ret
        }

                
ball_zp_len = * - ball_zp_copy

        
	+align256        
pf2_array:
	!for i, 50 {
        !byte $ff
        }
        !for i, 4 {
        !byte %11111110
        }
        !for i, 4 {
        !byte %11111100
        }
        !for i, 4 {
        !byte %11111000
        }
        !for i, 4 {
        !byte %11110000
        }
        !for i, 4 {
        !byte %11100000
        }
        !for i, 4 {
        !byte %11000000
        }
        !for i, 4 {
        !byte %10000000
        }
        !for i, 32 {
        !byte 0
        }
        
	+align256        

pf1_array:
	!for i, 18 {
        !byte $ff
        }
        !for i, 4 {
        !byte %01111111
        }
        !for i, 4 {
        !byte %00111111
        }
        !for i, 4 {
        !byte %00011111
        }
        !for i, 4 {
        !byte %00001111
        }
        !for i, 4 {
        !byte %00000111
        }
        !for i, 4 {
        !byte %00000011
        }
        !for i, 4 {
        !byte %00000001
        }
        !for i, 32 {
        !byte 0
        }

ball54:
	!src "ball/ball54.asm"
ball56:
	!src "ball/ball56.asm"
ball58:
	!src "ball/ball58.asm"
ball60:
	!src "ball/ball60.asm"
ball62:
	!src "ball/ball62.asm"
ball64:
	!src "ball/ball64.asm"
ball66:
	!src "ball/ball66.asm"
ball68:
	!src "ball/ball68.asm"
ball70:
	!src "ball/ball70.asm"
ball72:
	!src "ball/ball72.asm"
ball74:
	!src "ball/ball74.asm"
ball76:
	!src "ball/ball76.asm"
ball78:
	!src "ball/ball78.asm"
ball80:
	!src "ball/ball80.asm"
ball82:
	!src "ball/ball82.asm"
ball84:
	!src "ball/ball84.asm"
ball86:
	!src "ball/ball86.asm"
ball88:
	!src "ball/ball88.asm"
ball90:
	!src "ball/ball90.asm"
ball92:
	!src "ball/ball92.asm"
ball94:
	!src "ball/ball94.asm"
ball96:
	!src "ball/ball96.asm"
ball98:
	!src "ball/ball98.asm"
ball100:
	!src "ball/ball100.asm"
         
	+align256        

ballmove_neg:
	!for i, 8 {
        !byte 0
        }
        
sprmove_neg:
	!for i, 256-8-8-17-24*2 {
        !byte 0
        }
ballmove:
	!byte $00
        !byte $f0
        !byte $e0
        !byte $d0
        !byte $c0
        !byte $b0
        !byte $a0
        !byte $90
        !byte $80

sprmove:
	!byte $00
        !byte $10
        !byte $20
        !byte $30
        !byte $40
        !byte $50
        !byte $60
        !byte $70

balltable:
	!word ball54
	!word ball56
	!word ball58
	!word ball60
	!word ball62
	!word ball64
	!word ball66
	!word ball68
	!word ball70
	!word ball72
	!word ball74
	!word ball76
	!word ball78
	!word ball80
	!word ball82
	!word ball84
	!word ball86
	!word ball88
	!word ball90
	!word ball92
	!word ball94
	!word ball96
	!word ball98
	!word ball100
balltable_size = * - balltable

!if (balltable_size != 24*2) {
	!error "adjust 24*2 at sprmove_neg"
}

	;ballmove_neg at index $f8
        !byte 0
        !byte $70
        !byte $60
        !byte $50
        !byte $40
        !byte $30
        !byte $20
        !byte $10

	;sprmove_neg at index $f8
        !byte $80
        !byte $90
        !byte $a0
        !byte $b0
        !byte $c0
        !byte $d0
        !byte $e0
        !byte $f0

b_text = * + 122	;126 maximum value possible, before read of fff8 is reached
	+align256

	!src "ball/text.asm"
        
!if >ballmove_neg != >ballmove {
	!error "must be on same page"
}

!if >sprmove_neg != >sprmove {
	!error "must be on same page"
}

!if <sprmove != $c0 {
	!error "adjust <sprmove and lsr"
}

