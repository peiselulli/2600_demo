        STARTPIC_RAM = ram_start
        
SP_TIME = $20

--	jmp WaitForVblankEnd

startpic:
	lda initflag
        bne no_init_startpic
        ldx #startpic_len-1
-       lda startpic_copy,x
	sta STARTPIC_RAM,x
        dex
        bpl -
	
	lda #$0f
	sta AUDF0
	lda #$05
	sta AUDC0
	
        lda #SP_TIME
        sta part_duration
	lda #$03
	sta NUSIZ0
	sta NUSIZ1
	lda #$3f
	ldx #$00
	jsr PosObject
        lda #$47
        ldx #$01
	jsr PosObject
        lda #$01
        sta VDELP0
        sta VDELP1
        sta WSYNC
        sta HMOVE
        lda #$00
        sta GRP0
        sta GRP1
        sta GRP0
        ;lda #$ff
        ;sta COLUP0
        ;sta COLUP1
no_init_startpic:
	lda part_duration
	cmp #SP_TIME-1
	bcs --
	ldx #$00
	cmp #SP_TIME-2
	bcc +
	ldx #$0f	
+       stx AUDV0
	ldy #63
	sty tmp1
	lda sp_colors+63
	sta COLUP0
	sta COLUP1
	jsr WaitForVblankEnd
	ldx #78
-	sta WSYNC
	dex
	bne -
	ldx #13
-	dex
	bne -
	cmp ($00,x)
        jmp STARTPIC_RAM

startpic_copy:
!pseudopc STARTPIC_RAM {             
startpic_loop:
        lda sp_data1,y
        sta GRP0
        lda sp_data2,y
        sta GRP1
        lda sp_data3,y
        sta GRP0
        lda sp_data4,y
        sta <(startpic_data+1)
        ldx sp_data5,y
        lda sp_data6,y
startpic_data:
	ldy #$00
        sty GRP1
        stx GRP0
        sta GRP1
        sta GRP0
        ldy tmp1
	dey
	lda sp_colors,y 
        sta COLUP0
	sta COLUP1
        dec tmp1
        ldy tmp1
	bpl startpic_loop
        iny
        sty GRP0
        sty GRP1
        sty GRP0
        rts
}
startpic_len = * - startpic_copy
	+align256
	!src "startpic/pic.asm"
        
