SCENE_TEXT = 1
SCENE_MIRROR_COL = 2
SCENE_MIRROR = 3
SCENE_TURN   = 4
SCENE_SCROLL = 5

SCENE_CALC_VALS = $f0

MIRRORCOLOR = $c0

DELAY = 50
DELAY1 = 70
YPOS = 147+14

STARTSTATE = 1

;====================================
; static ZP adresses
;====================================
  !ifdef enable_logo {
static_vars = ram_start+$b4-$8a-10
dynamic_vars = ram_start+$18
  } else {
static_vars = ram_start
dynamic_vars = ram_start+$4a
  } 
RAM_pos0=static_vars
RAM_pos1=RAM_pos0+8 ;88
RAM_pos2=RAM_pos1+8 ;90
RAM_pos3=RAM_pos2+8 ;98
RAM_pos4=RAM_pos3+8 ;a0
RAM_pos5=RAM_pos4+8 ;a8
RAM_pos6=RAM_pos5+8 ;b0-b8


;cant remove those easily, should be possible in principle though
textwobble1=RAM_pos6+8
textwobble2 = RAM_pos6+9

state=RAM_pos6+10
ptr_text=RAM_pos6+11
flipframes=RAM_pos6+13
count_font_width=RAM_pos6+14

;====================================
; dynamic ZP adresses
;====================================

;based on frame counter now, check jmp Mainloop

;this COULD be coupled to the frame counter
;probably not worth it :)
;update: DONE ;-)

;to free 'toggle', we'd have to fine tune wobble_table1
;so '0' appears every 2^n frames. Then toggle can be
;set via frames as well
;but toggle is just 1 bit! it could be stored  in some PF0 or such
;update: DONE ;-) now based on flipframes, finetuned to wobble_table1
toggle=RAM_pos6+15 ; dynamic_vars

flag= dynamic_vars;
flag2=flag+1 
bwaiter=flag+2
bwaiter3=flag+3
wobblebuffer=flag+4
btemp2=flag+5
frames=flag+6

;====================================
scroll:
	!ifdef enable_logo {
        sty logo_stackmerk
        }
	lda initflag
        beq +
        jmp scroll_noinit
+
	ldx #$46-1+9
        lda #$00
-	sta static_vars,x
	dex
        bpl -
        ldx #$09
-	sta <dynamic_vars,x
	dex
        bpl -
 	lda #$d4
        sta part_duration
	;lda #$00
;init delays here 2013 FIX?
  lda #4
  sta textwobble1
  lda #4
  sta <bwaiter
  sta <bwaiter3
  lda #2
  sta <textwobble2
  lda #<TEXT
  sta <ptr_text
  lda #>TEXT
  sta <(ptr_text+1)
;  lda #(wobble_table_end-wobble_table1)
 ; sta turner

  lda #7 
  sta <count_font_width

  lda #1
  sta <toggle

  
  ;lda #0
  ;sta RAM_pos0+0
  ;sta RAM_pos0+1
  ;sta RAM_pos0+2
  ;sta RAM_pos0+3
  ;sta RAM_pos0+4
  ;sta RAM_pos0+5
  ;sta RAM_pos0+6
  ;sta RAM_pos0+7
  lda #SCENE_CALC_VALS
  sta state
  
  !ifdef enable_logo {
  jmp scroll_end
  } else {
  jmp WaitForVblankEnd
  }


;fill ram with 40 chars
;takes > 1 frame!

scroll_noinit:
  lda initflag
  and #$07
  sta <frames
  lda #$ff
  sec
  sbc part_duration
  asl
  asl
  asl
  ora <frames
  sta <frames

;debugging	
  lda #0
  sta <flag
  sta <flag2
  sta <bwaiter
  sta <bwaiter3
  ;sta <wobblebuffer
  sta <btemp2

  lda $e2
  cmp #$f9
  bne +
  nop
+
  lda state
  cmp #SCENE_CALC_VALS
  bcc +
  !ifdef enable_logo {
  } else {
  jsr WaitForVblankEnd
stabilizer2:
  lda INTIM
  cmp #$08
  bne stabilizer2
  }
  ldx #8
textinitloop
  txa
  pha
  jsr scroller
  pla
  tax
  dex
  bne textinitloop
  inc state
  lda state
  cmp #SCENE_CALC_VALS+5
  bne ++
  lda #STARTSTATE
  sta state
++  
  jmp scroll_end
  
+
  ;inc frames
 
  lda #0
  sta COLUBK

  ;lda #2
  ;sta VSYNC 
  ;sta WSYNC
  ;sta WSYNC
  ;sta WSYNC
  
  ;lda #0
  ;sta VSYNC
;==============================================
  ;lda #47
  ;sta TIM64T  ;scanlines vertical blend

  lda state ;check state of the effect
  cmp #SCENE_SCROLL
  bcc dontscroll
  jsr scroller  ;scroll text here or just wait
dontscroll

 sta WSYNC

  !ifdef enable_logo {
  } else {
 jsr WaitForVblankEnd
 }
;==========================================
  ;lda #YPOS ;y pos of scroller
  ;sta TIM64T  ;just waste verticale space here

done_with_screen
  
lda #0
  sta COLUBK

;do we want to turn the text?
  lda state
  cmp #SCENE_TURN
  bcc dontturnit
  bcs turnit ;if >
dontturnit
  jmp skipturn
turnit

  lda flipframes
  inc flipframes

;set toggle here, i.e. if flipframes < 10 then toggle = 1
  cmp #50
  bcc notc

  ldx #0
  stx <toggle
  beq yesc

notc
  ldx #1
  stx <toggle
yesc

  cmp #(wobble_table_end-wobble_table1)
  bne thisisok

  lda #0
  sta flipframes

  
thisisok
  tax ;'frame'-counter, or event counter rather
  lda wobble_table1,x ;0-6

  clc
  adc #1 ;1-7
isok2
  sta <wobblebuffer
  sta textwobble1 ;height of pixel for BIG letters
  lsr
  clc
  adc #1 ;so its never 0
  sta textwobble2 ;height of pixel for small mirror letters

  lda <wobblebuffer
  cmp #1
  bne nothing


 ; lda toggle
 ; eor #1
  ;sta toggle
nothing
skipturn
;-----------------

;bwaiter	;X
;"p"		;7*textwobble1
;bwaiter	;X
;---------
;bwaiter3	;Y
;"b"		;7*textwobble2
;bwaiter3	;Y

; 7*textwobble1 + 2*x = const = 7*7+2 = 51

; 7*textwobble2 + 2*y = const = 7*4+2 = 30

  lda #51
  sec
  sbc <wobblebuffer
  sbc <wobblebuffer
  sbc <wobblebuffer
  sbc <wobblebuffer
  sbc <wobblebuffer
  sbc <wobblebuffer
  sbc <wobblebuffer
  lsr
  sta <bwaiter

  lda #22;30-2
  sec
  sbc <wobblebuffer
  sbc <wobblebuffer
  sbc <wobblebuffer
  lsr
  sta <bwaiter3

stabilizer:
 lda INTIM
 cmp #$08
 bne stabilizer

 ;lda #DELAY1 ;set stable spacing for mirror edge
 ; sta TIM64T  

  ;just debugging stuff, this block can be deleted
  lda #0
  sta VDELP0
  sta VDELP1
  sta ENAM0
  sta ENAM1
  sta ENABL
  sta GRP0
  sta GRP1
  sta PF0

  lda #%00111000
  sta CTRLPF
  sta WSYNC

  lda state
  cmp #SCENE_TEXT
  bcs continue1 
  jmp in_state0
continue1
loop2

;timing for stable y-size despite the changing letter height
  ldx <bwaiter
waiter
  sta WSYNC
  dex
  bne waiter

uskip ;decide if we are showing upside down or not
  lda <toggle
  bne normal
  jmp ucont	;both rows are y-flipped
normal
  jmp cont

!fill ((*+$100)/256)*256-*,0
;------------------
;* normal text upper row  
cont
      sta WSYNC
      lda #$98
      sta COLUPF

  
       ldx    #$00    ;counter for letter-pixels
	sta WSYNC
        lda    RAM_pos6,x   
ll1 
      ldy    textwobble1    ;height of letter-pixel
ll2 
       sta    PF0     
       lda    RAM_pos5,x   
       sta    PF1     
       lda    RAM_pos4,x   
       sta    PF2     
       inc    $00  ; dummy 5 cycles
       lda    RAM_pos3,x   
       sta    PF0     
       lda    RAM_pos2,x   
       sta    PF1     
       lda    RAM_pos1,x   
       sta    PF2     

	lda color_table1,x
	adc color_table2,y
        sta COLUPF	

       lda    RAM_pos6,x   ;needed for the pf0 to come
       sta    WSYNC   
	
       dey            
       bne    ll2   
       inx           
 
       cpx    #$07    ;chars are 7 pix high
       bne    ll1   

;-----------------------------------------------
      lda #$cc
      sta COLUPF
	lda    #$00    ;blank PF again
       sta    PF0     
       sta    PF1     
       sta    PF2    
     sta WSYNC
;-----------------------------------------------
;get a stable mirror-line via timer
stabilizea:
  sta WSYNC
  lda INTIM
  cmp #$03
  bne stabilizea
 sta WSYNC
 ;lda #DELAY 
 ;sta TIM64T 

todo ;optionally use ZP adress here to toggle mirror on/off as well
  lda #MIRRORCOLOR
  sta COLUBK

;do we even want to show the mirror?
lda state
  cmp #SCENE_MIRROR
  bcc skipmirror

  sta WSYNC

  ldx <bwaiter3
  beq nowaita
waiter3a
  sta WSYNC
  dex
  bne waiter3a
nowaita

 sta WSYNC
;this is the 2nd row, mirrored text

       ldx    #$06    ;counter for letter-pixels
        lda    RAM_pos6,x   
mll1 ldy    textwobble2    ;height of letter-pixel
mll2 
       sta    PF0     
       lda    RAM_pos5,x   
       sta    PF1     
       lda    RAM_pos4,x   
       sta    PF2     
       inc    $00  ; dummy
       lda    RAM_pos3,x   
       sta    PF0     
       lda    RAM_pos2,x   
       sta    PF1     
       lda    RAM_pos1,x   
       sta    PF2     
      lda color_table1,x
	adc color_table3,y

      sta COLUPF
        lda    RAM_pos6,x   
       sta    WSYNC   
       dey            
       bne    mll2 
        dex
      bpl    mll1

skipmirror     
      jmp textdone
;---------------------------------
;this is for the mirrored case!
;first row is upside down
!fill ((*+$100)/256)*256-*,0
ucont
  sta WSYNC

       ldx    #$06    ;counter for letter-pixels
    sta WSYNC
        lda    RAM_pos6,x   
ull1 ldy    textwobble1    ;height of letter-pixel
ull2 
       sta    PF0     
       lda    RAM_pos5,x   
       sta    PF1     
       lda    RAM_pos4,x   
       sta    PF2     
       inc    $00  ; dummy 5 cycles
       lda    RAM_pos3,x   
       sta    PF0     
       lda    RAM_pos2,x   
       sta    PF1     
       lda    RAM_pos1,x   
       sta    PF2     
      
utest
	lda color_table1,x
	adc color_table2,y
        sta COLUPF	

       lda    RAM_pos6,x
       sta    WSYNC   
	
       dey            
       bne    ull2
        dex
        bpl    ull1     

;-----------------------------------------------
      lda #$98
      sta COLUPF
      lda    #$00
       sta    PF0     
       sta    PF1     
       sta    PF2    
      sta WSYNC
;-----------------------------------------------
;the mirrored part
stabilizeb
  sta WSYNC
  lda INTIM
  cmp #$03
  bne stabilizeb

  sta WSYNC
  ;lda #DELAY
  ;sta TIM64T
  lda #MIRRORCOLOR
  sta COLUBK

;do we even want to show the mirror?
lda state
  cmp #SCENE_MIRROR
  bcc skipmirror2

sta WSYNC

  ldx <bwaiter3
  beq nowaitb
uwaiter3b
  sta WSYNC
  dex
  bne uwaiter3b
nowaitb


	sta WSYNC
       ldx    #$00
       lda    RAM_pos6,x   
umll1 
      ldy    textwobble2
umll2 
       sta    PF0
       lda    RAM_pos5,x   
       sta    PF1     
       lda    RAM_pos4,x   
       sta    PF2     
       inc    $00  ; dummy
       lda    RAM_pos3,x   
       sta    PF0     
       lda    RAM_pos2,x   
       sta    PF1     
       lda    RAM_pos1,x   
       sta    PF2     
       lda color_table1,x
       adc color_table3,y
       sta COLUPF
       lda    RAM_pos6,x   
       sta    WSYNC   
       dey            
       bne    umll2
       inx            
       cpx    #$07
       bne    umll1 
;---------------------------------
skipmirror2
textdone

       lda    #$00
       sta    PF0     
       sta    PF1     
       sta    PF2     
 

;----------------------------
in_state0
doit
skip
  sta WSYNC

;WaitForTimer
;  lda INTIM
;- cmp INTIM  
;  beq -

  sta WSYNC ;clean end of rasterline after timer
;=================================
;set new timer
  ;lda #41
  ;sta TIM64T


;---------------
  sta WSYNC

WaitForTimer2
;  lda INTIM
;  cmp #$02  
;  bne WaitForTimer2
;  sta WSYNC
 
lda #0
  sta ENAM0
  lda <frames
;2012 only to speed up state-changes for testing
  and #%00111111
  bne ok7
  inc state
  lda state
  cmp #SCENE_CALC_VALS
  bne ok7
  dec state
ok7


;use frame pointer for text pointer magic ;-)
  lda <frames
  and #%00000111
  sta <count_font_width

  ;lda <frames
  ;lsr
  ;lsr
  ;sta <ptr_text

scroll_end:
  !ifdef enable_logo {
  ldx logo_stackmerk
  txs
  }
  rts
  
CHARS
;.bin 2,0,"testmap.bin"
!binary "scroll/testmap.bin",,2

;MUST be within 1 page
!fill ((*+$100)/256)*256-*,0

;----------------------------------------
scroller
    ldx    #$06 
innerloop clc            
       lda    RAM_pos0,x   
       rol            
       sta    RAM_pos0,x   
       lda    RAM_pos1,x   
       ror            
       sta    RAM_pos1,x   
       lda    RAM_pos2,x   
       rol            
       sta    RAM_pos2,x   
       lda    RAM_pos3,x   
       ror            
       sta    RAM_pos3,x   
       ror    ;instead of carry branches just ror through        
       ror            
       ror            
       ror            
       lda    RAM_pos4,x   
       ror            
       sta    RAM_pos4,x   
       lda    RAM_pos5,x   
       rol            
       sta    RAM_pos5,x   
       lda    RAM_pos6,x   
       ror            
       sta    RAM_pos6,x   
       dex
       bpl    innerloop

       inc    <count_font_width     ;how many times did we need to scroll THAT char?
    ;ENTER proportional font here ;-))
       lda    <count_font_width     
      sec
      sbc #8
       bne    endc   
       sta    <count_font_width     
	jsr getchar
	jsr getchar
endc
	rts
;----------------------------------------
getchar

  ;get a char and return it as low nibble

       ldy #0  
       sty <flag ;init flag
      ;ldy <ptr_text
      lda (ptr_text),y
      cmp #$ff ;endmarker
      bne okb
      lda #<(TEXT)
      sta <ptr_text
      lda #>(TEXT)
      sta <(ptr_text+1)
      lda TEXT
      ;sta <ptr_text ;=0
okb   inc <ptr_text
      bne +
      inc <(ptr_text+1)
+

oka
      lsr ;carry=left(0) or right(1)?
      rol <flag
      ldx <flag
      beq getleft
      jmp getright

getleft
      ldx #0
       sta <btemp2
       asl
       asl            
       clc
       adc <btemp2
       adc <btemp2
       adc <btemp2
       tay            
       lda <flag2 ;put left/right
       bne right
; and store left
left
lloop  lda    CHARS,Y ;get bitmap of both chars
       and #$f0
       sta    RAM_pos0,x      
       iny            
       inx            
       cpx    #$07    
       bne    lloop
       ;inc    <ptr_text
       jmp endthis
; and store right
right
rloop 
	lda    CHARS,Y 
	lsr
	lsr
	lsr
	lsr
       ora    RAM_pos0,x ;right one is always overlap
        sta    RAM_pos0,x      
       iny            
       inx            
       cpx    #$07    
       bne    rloop
       ;inc    <ptr_text

endthis			;toggle left/right
    lda <flag2
    eor #01
    sta <flag2
      rts
;----------------------------
getright
      ldx #0
      ;get left side
      sta <btemp2
      asl
       asl
       clc
       adc <btemp2
       adc <btemp2
       adc <btemp2
       tay
      lda <flag2 ;pout left/right
      bne bright
; and store left
bleft
blloop  lda    CHARS,Y ;get bitmap of both chars
       asl
      asl
      asl
      asl
       sta    RAM_pos0,x      
       iny            
       inx            
       cpx    #$07    
       bne    blloop
       ;inc    <ptr_text
       jmp bendthis
; and store left
bright
brloop 
	lda    CHARS,Y 
      and #$0f
       ora    RAM_pos0,x ;right one is always overlap
        sta    RAM_pos0,x      
       iny            
       inx            
       cpx    #$07    
       bne    brloop
       ;inc    <ptr_text

bendthis			;toggle left/right
    lda <flag2
    eor #01
    sta <flag2
      rts



;----------------------------------------
!fill ((*+$100)/256)*256-*,0

wobble_table1
!byte 0,0,1,1,1,1,2,2,3,4,4,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6
!byte 6,5,5,5,4,4,3,2,2,1,1,1,1,0,1,1,1,2,2,3,4,5,5,6,6,6,5,5,4,3,2,2,1,1,1
wobble_table_end



color_table2
!byte  2, 6,10,12,14,12,10, 6, 2

color_table3
!byte  0, 2, 4, 6, 8, 6, 4, 2, 0

color_table1
!byte $30,$50,$70,$90,$b0,$d0,$c0,$a0


TEXT

!binary "scroll/textptr.dat"
!byte $ff ;endmarker


 ;-----------------------------


