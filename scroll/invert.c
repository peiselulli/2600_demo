#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
	unsigned char memory[65536];
        int size;
        FILE *f;
        int i;
        
	if(argc < 2)
        {
        	printf("usage: %s <file>\n", argv[0]);
                exit(-1);
        }
        f = fopen(argv[1], "rb");
        if (!f)
        {
        	printf("cannot open %s\n", argv[1]);
                exit(-1);
        }
        size = fread(memory, 1, sizeof(memory), f);
        fclose(f);
        for(i=0; i < size; ++i)
        {
        	memory[i] = 0xff - memory[i];
        }
        f = fopen(argv[1], "wb");
        fwrite(memory, 1, size, f);
        fclose(f);
	return 0;
}
