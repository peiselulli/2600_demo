#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PNG2HIRES v0.1 - gfx format converter /enthusi (onslaught)
# (c) Martin 'enthusi' Wendt 04/2012
# feel free to distribute enhanced/changed and credited versions :)

import sys, struct
from PIL import Image
import math
import base64

c64pal=\
[(0x00,0x00,0x00),
(0xFF,0xFF,0xFF),
(0x9a,0x53,0x48),
(0x8c,0xc9,0xd1),
(0x9b,0x5a,0xbb),
(0x7b,0xb7,0x54),
(0x51,0x42,0xb1),
(0xd8,0xe2,0x84),
(0xa0,0x72,0x35),
(0x70,0x5b,0x00),
(0xc6,0x89,0x80),
(0x69,0x69,0x69),
(0x92,0x92,0x92),
(0xb9,0xee,0x99),
(0x8e,0x82,0xe1),
(0xb9,0xb9,0xb9)]

#gimp palette
c64pal2=\
[(0x00,0x0,0x00),
(0xFF,0xFF,0xFF),
(0x68,0x37,0x2B),
(0x70,0xA4,0xB2),
(0x6F,0x3D,0x86),
(0x58,0x8D,0x43),
(0x35,0x28,0x79),
(0xB8,0xC7,0x6F),
(0x6F,0x4F,0x25),
(0x43,0x39,0x00),
(0x9A,0x67,0x59),
(0x44,0x44,0x44),
(0x6C,0x6C,0x6C),
(0x9A,0xD2,0x84),
(0x6C,0x5E,0xB5),
(0x95,0x95,0x95)]

#other pal
c64pal3=\
[(0x00,0x00,0x00),
(0xff,0xff,0xff ),
(0x88,0x00,0x00 ),
(0xaa,0xff,0xee ),
(0xcc,0x44,0xcc ),
(0x00,0xcc,0x55 ),
(0x00,0x00,0xaa ),
(0xee,0xee,0x77 ),
(0xdd,0x88,0x55 ),
(0x66,0x44,0x00 ),
(0xff,0x77,0x77 ),
(0x33,0x33,0x33 ),
(0x77,0x77,0x77 ),
(0xaa,0xff,0x66 ),
(0x00,0x88,0xff ),
(0xbb,0xbb,0xbb )]

#ADD YOUR PAL HERE 
palname=['ViceBMP','GIMP','unknown']
c64pals=[c64pal,c64pal2,c64pal3]
#----------------------------------------------------------
def best_match(incol,rgblist,mode):
  if mode=='rgb':
      delta=0
      c1,c2,c3=incol
      rgbdiff=99999999
      for i in range(len(rgblist)):
        r,g,b=rgblist[i]
        drgb=(c1-r)**2+(c2-g)**2+(c3-b)**2
        if drgb<rgbdiff:
          rgbdiff=drgb
          delta=rgbdiff
          ctouse=i
      return rgblist[ctouse],delta
      
  if mode=='yuv':
      c1,c2,c3=incol
      Y1 =  0.299*c1 + 0.587*c2 + 0.114*c3
      U1 = -0.147*c1 - 0.289*c2 + 0.436*c3
      V1 =  0.615*c1 - 0.515*c2 - 0.100*c3
      yuvdiff=99999999
      for i in range(len(rgblist)):
        r,g,b=rgblist[i]
        Y2 =  0.299*r + 0.587*g + 0.114*b
        U2 = -0.147*r - 0.289*g + 0.436*b
        V2 =  0.615*r - 0.515*g - 0.100*b
        dyuv=3.0*abs(Y1-Y2) + math.sqrt((U1-U2)**2 + (V1-V2)**2)
        if dyuv<yuvdiff:
          yuvdiff=dyuv
          ctouse=i
      return rgblist[ctouse]
#----------------------------------------------------------
def fast_toc64pal(non64pal):
  #provide 256 entry-c64 palette
  global manual_pal
  global ega_pal
  bestqual=99999999999999
  bestp=0
  pal_image= non64pal.convert('P',palette=Image.ADAPTIVE,dither='none',colors=16)
  thispal=pal_image.getpalette()
  #thispal has 3(rgb)*16 entries, then filled up with 0 to 3*256
  for p in range(len(c64pals)):
      quality=0
      palette = []
      for i in range(16):
        palette.extend(c64pals[p][i])
      for i in range(240):
        palette.extend((0,0,0))
      for i in range(0,16*3,3):
        c_r=thispal[i]
        c_g=thispal[i+1]
        c_b=thispal[i+2]
        foundcol,delta=best_match((c_r,c_g,c_b),c64pals[p],'rgb')
        quality+=delta
      if quality<bestqual:
        bestp=p
        bestqual=quality
  p=bestp
  quality=0
  palette = []
  thispal=pal_image.getpalette()
  for i in range(16):
    palette.extend(c64pals[p][i])
  for i in range(240):
    palette.extend((0,0,0))
  for i in range(0,256*3,3):
    c_r=thispal[i]
    c_g=thispal[i+1]
    c_b=thispal[i+2]
    foundcol,delta=best_match((c_r,c_g,c_b),c64pals[p],'rgb')
    quality+=delta
    thispal[i]=foundcol[0]
    thispal[i+1]=foundcol[1]
    thispal[i+2]=foundcol[2]
  pal_image.putpalette(thispal)
  pal_image=pal_image.convert('RGB')
  ega_pal=thispal[:]
  if quality==0:
    print "perfect palette match for %s-palette" % palname[p]
  else:
    print "Quality: %d for %s-palette " %(quality,palname[p])
  return pal_image,bestp
#----------------------------------------------------------
def read_in_gfxRGB(gfxname):
    print "-------------------"
    gfx = Image.open(gfxname).convert('RGB').convert('P', palette=Image.ADAPTIVE)
    colors = len(filter(None, gfx.histogram()))
    p=0
    if colors==2:
      mode=0
      print "SingleColor HiRes '%s'"%gfxname
    else:
      mode=1
      print "MultiColor HiRes '%s'"%gfxname
      print "%d colors used"%colors
    w,h=gfx.size
    if (w%8 != 0):
      print "gfx width not a multiple of 8 (%d)" % w
      sys.exit(1)
    if (h%8 != 0):
      print "gfx height not a multiple of 8 (%d)" % h
      sys.exit(1)

    cw=w/8
    ch=h/8
    i=0
    bitmap=[]
    colmap=[]
    #bitmap.append(cw)
    #bitmap.append(ch)
    #bitmap.append(mode)
    if mode==0:
        for cy in range(0,ch):
            for cx in range(0,cw):
              box=(cx*8,cy*8,cx*8+8,cy*8+8)
              square=gfx.crop(box)
              for y in range(0,8):
                line=0
                i+=1
                for x in range(0,8):
                  colnow=square.getpixel((x,y))
                  line+=colnow* 2**(7-x)
                bitmap.append(line)
        for i in range(1000):
	  colmap.append(1)
        return bitmap,colmap

    if mode==1:
      bitmap_data=[]
      color_data=[]
      clash=0
      image = Image.open(gfxname).convert('RGB')
      new,p=fast_toc64pal(image)
      for cy in range(ch):
          for cx in range(cw):
            box=(cx*8,cy*8,cx*8+8,cy*8+8)
            square=new.crop(box)
            fcol=100
            bcol=100
            char_col=[]
            for y in range(0,8):
              line=0
              for x in range(0,8):
                colnow=square.getpixel((x,y))
                tcol=c64pals[p].index(colnow)
                if tcol not in char_col:
                  char_col.append(tcol)
                colptr=char_col.index(tcol)
                line+=colptr * 2**(7-x)
              bitmap.append(line)
              bitmap_data.append(line)
            if len(char_col)==1:
              char_col.append(char_col[0])
            bitmap.append(char_col[1]*16+char_col[0])
            color_data.append(char_col[1]*16+char_col[0])
            if len(char_col)>2:
              clash=1
              print "Too many colors per block in char %d, %d near x=%d, y=%d." % (cx,cy,cx*8+4,cy*8+4)
              for xa in range(cx*8,cx*8+8):
                r,g,b=image.getpixel((xa,cy*8))
                image.putpixel((xa,cy*8),(r*2,g/2,b/2))
                r,g,b=image.getpixel((xa,cy*8+7))
                image.putpixel((xa,cy*8+7),(r*2+0x80,g/2,b/2))
              for ya in range(cy*8,cy*8+8):
                r,g,b=image.getpixel((cx*8,ya))
                image.putpixel((cx*8,ya),(r+0x100,g/2,b/2))
                r,g,b=image.getpixel((cx*8+7,ya))
                image.putpixel((cx*8+7,ya),(r+0x100,g/2,b/2))
      if clash:
        clashes=image.resize((640,400))
        clashes.show()
        sys.exit(0)
      else:
        print "Conversion successful."
        return bitmap_data,color_data
      
print "PNG2HIRES v0.1 - gfx format converter /enthusi (onslaught)\nUsage: python png2hires.py image.png"
bitmap,colmap=read_in_gfxRGB(sys.argv[1])
hires_file=open ('%s.ip64h'% (sys.argv[1][:-4]),'wb')
bitmap_file=open ('%s.bitmap'% (sys.argv[1][:-4]),'wb')
colmap_file=open ('%s.colmap'% (sys.argv[1][:-4]),'wb')
binary_file=open ('%s.prg'% (sys.argv[1][:-4]),'wb')
#write hires frame
hires_file.write('%c'%0x00)
hires_file.write('%c'%0x60)
for a in bitmap:
  hires_file.write('%c'%a)
for a in colmap:
  hires_file.write('%c'%a)
#write bitmap ($6000)+ colmap ($5c00) data
bitmap_file.write('%c'%0x00)
bitmap_file.write('%c'%0x20)

colmap_file.write('%c'%0x00)
colmap_file.write('%c'%0x0c)

for a in bitmap:
  bitmap_file.write('%c'%a)
for a in colmap:
  colmap_file.write('%c'%a)
#write prg-file
viewer_binary=base64.b64decode("""AQgLCAAAnjIwNjEAAAB4qbuNEdCpPI0Y0EwYCGVudGh1c2kvb25zbGF1Z2h0IDA0LzIwMTI=""")
for a in viewer_binary:
  binary_file.write('%c'%a)
for i in range(0x401-len(viewer_binary)):
  binary_file.write('%c'%0x00)
for a in colmap:
  binary_file.write('%c'%a)
for i in range(0x1018):
  binary_file.write('%c'%0x00)
for a in bitmap:
  binary_file.write('%c'%a)
  
  

