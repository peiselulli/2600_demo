#!/usr/bin/env python
# -*- coding: utf-8 -*-
#art-trosis
letters=""" !"#$%&^()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]|_'abcdefghijklmnopqrstuvwxyz[|]üöäß"""

msg="""  aTaRSI    you have reached the end of this demo by TRSI with the help of DSS and CREST ...            Code:\
    Peiselulli of TRSI ...\
            Music:    Tjoppen of DSS ...             GFX:    Deekay of Crest ...          \
this scroller by Enthusi of Onslaught ...          \
 Released by TRSI at the Nordlicht 2013 party.        Demo restarts now    ...                    """

bitmap=open("chars2.bitmap","rb").read()[2:]

chars_used=[]
ptrs=[]
fontbig=[]
cbmap=[]

for char in msg: #parse message
  if char not in chars_used: #find different chars
    chars_used.append(char)
    #add the used charbitmap
    pos=letters.index(char)
    cbmap=[]
    for a in range(8):
      cbmap.append(bitmap[pos*8+a])
    fontbig.append(cbmap)
    
for char in msg: #find pointers into used chars
  ptr=chars_used.index(char)
  ptrs.append(ptr)
print '"%s"' % msg
print chars_used

print len(msg),len(chars_used),max(ptrs)
print len(fontbig)#,fontbig

test=open('testmap.bin','w')
test.write("%c%c" % (0x00,0xe0))

#make sure we have n*2 chars
if len(fontbig)%2==1:
  fontbig.append(cbmap) #cbmap should still be defined
  
#now, pointers stay but render new chars with 4 bit width
font=[]
for a in range(len(fontbig)/2):
  #print a
  gfxh=fontbig[a*2+0]
  gfxl=fontbig[a*2+1]
  gfxb=[]
  for b in range(8):
    left=ord(gfxh[b])&0xf0
    right=ord(gfxl[b])&0x0f
    both=left+right
    newv=("%c" % both)
    if b>0: #row1-7
      gfxb.append(newv)
  font.append(gfxb)
  
print len(font)
    
for g in font:
  for a in range(7):
    test.write("%c"%g[a])
    
pfile=open("textptr.dat","w")
for p in ptrs:
  pfile.write("%c"%p)

print ptrs
