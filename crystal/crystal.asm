c_addvalue	= ram_start
c_base_x	= ram_start+1
c_x_start = ram_start+2

C_x_diff1 = ram_start+3
C_x_val1 = ram_start+4
C_x_dir1 = ram_start+5

C_x_diff2 = ram_start+6
C_x_val2 = ram_start+7
C_x_dir2 = ram_start+8

C_x_diff3 = ram_start+9
C_x_val3 = ram_start+10
C_x_dir3 = ram_start+11

C_x_diff4 = ram_start+12
C_x_val4 = ram_start+13
C_x_dir4 = ram_start+14

C_x_diff5 = ram_start+15
C_x_val5 = ram_start+16
C_x_dir5 = ram_start+17
c_index = ram_start+18
c_size = ram_start+19
c_size_1 = ram_start+20
c_size_index = ram_start+21
c_delay = ram_start+22
c_pos_index = ram_start+23

crystal:
	lda initflag
        bne crystal_noinit
	lda #$60
	sta part_duration
	lda #$00
	sta c_pos_index
	sta c_size_index
	sta c_index
	sta c_base_x
	lda #$58
	sta c_x_start
	lda #$04
	sta CTRLPF
	ldx #$1c
	stx c_size

crystal_noinit:
	+skip_framewait
	lda #$00
	sta COLUP0
	sta COLUP1
	sta COLUPF

	ldy c_size_index
	ldx crystal_sizes,y
	stx c_size
	inx
	stx c_size_1
	lda #$1c
	sec
	sbc c_size
	sta c_delay
	inc c_size_index

	ldx c_pos_index
	lda crystal_xpos,x
	sta c_x_start
	lda c_size_index
	lsr
	bcc +
	inc c_pos_index
+
	inc c_pos_index
	lax c_index
	ldy crystal_diffs,x
	sty C_x_diff5
	ldy crystal_dirs,x
	sty C_x_dir5
	sbx #51
	ldy crystal_diffs,x
	sty C_x_diff1
	ldy crystal_dirs,x
	sty C_x_dir1
	txa
	sbx #51
	ldy crystal_diffs,x
	sty C_x_diff2
	ldy crystal_dirs,x
	sty C_x_dir2
	txa
	sbx #51
	ldy crystal_diffs,x
	sty C_x_diff3
	ldy crystal_dirs,x
	sty C_x_dir3
	txa
	sbx #51
	ldy crystal_diffs,x
	sty C_x_diff4
	ldy crystal_dirs,x
	sty C_x_dir4

	inc c_index
	lda c_index
	cmp #52
	bne +
	lda #$00
	sta c_index
+
	lda #$80
	sta C_x_val1
	sta C_x_val2
	sta C_x_val3
	sta C_x_val4
	sta C_x_val5

	lda #$f0
	sta c_addvalue

        ldx #$00
        lda c_x_start
        jsr PosObject

        ldx #$01
        lda c_x_start
        jsr PosObject

        ldx #$02
        lda c_x_start
	clc
	adc #$01
        jsr PosObject

        ldx #$03
        lda c_x_start
	clc
	adc #$01
        jsr PosObject

        ldx #$04
        lda c_x_start
	clc
	adc #$01
        jsr PosObject


	lda #$80
	sta GRP0
	sta GRP1
	lda #$02
	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #$00
	sta COLUBK
	sta WSYNC
	sta HMOVE
	jsr WaitForVblankEnd
	sta HMCLR
	ldx c_delay
	beq +
-	sta WSYNC
	dex
	bne -
+
	sta WSYNC
	!for i, 32 {
	nop
	}
	lda #$56
	sta COLUP1
	lda #$5e
	sta COLUP0
	sta COLUPF
	ldx c_size
	jsr paint_crystal
	jsr invert_crystal
	sta WSYNC
	ldx c_size_1
	jsr paint_crystal
	lda #$00
	sta GRP0
	sta GRP1
	sta ENAM0
	sta ENAM1
	sta ENABL
	jsr invert_crystal

-
	lda INTIM
	cmp #$08
	bne -
        sta WSYNC
        lda #$ff
        sta COLUBK
	ldx c_base_x
	lda #$80
	pha
-	pla
	clc
	adc c_addvalue
	bcc +
	inx
	clc
+	adc c_addvalue
	dec c_addvalue
	bcc +
	inx
	clc
+	adc c_addvalue
	dec c_addvalue
	bcc +
	inx
	clc
+	pha
	lda base_colortab,x
	sta WSYNC
	sta COLUBK
	lda INTIM	
	bne -
	sta WSYNC

	LDA #2		
	STA WSYNC  	
	STA VBLANK
        lda #40
        sta TIM64T
	dec c_base_x
	pla
	rts

invert_crystal:
	lda C_x_dir1
	eor #$e0
	sta C_x_dir1
	lda C_x_dir2
	eor #$e0
	sta C_x_dir2
	lda C_x_dir3
	eor #$e0
	sta C_x_dir3
	lda C_x_dir4
	eor #$e0
	sta C_x_dir4
	lda C_x_dir5
	eor #$e0
	sta C_x_dir5
	lda #$00
	sec
	sbc C_x_val1
	sta C_x_val1
	lda #$00
	sec
	sbc C_x_val2
	sta C_x_val2
	lda #$00
	sec
	sbc C_x_val3
	sta C_x_val3
	lda #$00
	sec
	sbc C_x_val4
	sta C_x_val4
	lda #$00
	sec
	sbc C_x_val5
	sta C_x_val5
	rts

paint_crystal:
-	lda C_x_val1
	clc
	adc C_x_diff1
	sta C_x_val1
	lda C_x_dir1
	ldy #$08
--	dey
	bne --
	sta HMCLR
	bcc +
	sta HMP0
+	lda C_x_val2
	clc
	adc C_x_diff2
	sta C_x_val2
	lda C_x_dir2
	bcc +
	sta HMM0
+	lda C_x_val3
	clc
	adc C_x_diff3
	sta C_x_val3
	lda C_x_dir3
	bcc +
	sta HMP1
+	lda C_x_val4
	clc
	adc C_x_diff4
	sta C_x_val4
	lda C_x_dir4
	bcc +
	sta HMM1
+	lda C_x_val5
	clc
	adc C_x_diff5
	sta C_x_val5
	lda C_x_dir5
	bcc +
	sta HMBL
+
	sta WSYNC
	sta HMOVE
	dex
	bne -
	rts

	+align256
base_colortab:
	!for i, 4 {
	!set k = i*2
	!for j, 16 {
		!byte $40+(j-1)+(k*16)
		!byte $40+(j-1)+(k*16)
	}
	!for j, 16 {
		!byte $40+(16-j)+(k*16)
		!byte $40+(16-j)+(k*16)
	}	
	}

	+align256
	!src "crystal/crysdata.asm"

