#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int i;
        int v;
        double a;
        FILE *f;
        
        f = fopen("sintab1", "wb");
	for (i=0; i < 256; ++i)
        {
        	a = sin((double)i / 128.0 * M_PI) * 64.0+64.0;
                v = (int)a;
                fputc(v, f);
        }
        fclose(f);
}
