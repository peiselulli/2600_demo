;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Street Tuff of Red Sector Inc. presents - Stuff on Atari 2600 #12 +
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	v_81 = ram_start
	v_82 = ram_start+1
	v_84 = ram_start+2
	v_87 = ram_start+4
	v_88 	= ram_start+5
	v_90 	= ram_start+5+8
	v_98 	= ram_start+5+16
	v_a0 	= ram_start+5+24
	v_a8 	= ram_start+5+32
	v_b0 	= ram_start+5+40
	v_b8 	= ram_start+5+48
	

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

tuffscroll:
	lda initflag
	bne tuffscroll_noinit
	lda #$80
        sta part_duration
	jsr tuffinit
tuffscroll_noinit:
	lda #$00
	sta CTRLPF
	lda #$00
	sta COLUBK
	lda #$00
	sta COLUPF
	jsr WaitForVblankEnd

;	dec COLUPF

	ldy v_87
	lax sinx1,y
	sbx #$100-8
waste	sta WSYNC
	dex
	bne waste
	
	inc v_87

	ldx #$00
cwaste1	lda colors,x
	sta COLUBK
	sta WSYNC
	inx
	cpx #$08
	bne cwaste1

	lda #$98
	sta COLUBK
	ldx #$09
waster	sta WSYNC
	dex
	bne waster

	ldx #$00
dloop1	
	ldy #$07
sizloop	lda v_b8,x
	sta PF0
	lda v_b0,x
	sta PF1
	lda v_a8,x
	sta PF2

	stx COLUPF
	lda v_a0,x
	sta PF0
	lda v_98,x
	sta PF1
	lda v_90,x
	sta PF2

	sta WSYNC
	dey
	bne sizloop

	inx
	cpx #8 ;62
	bne dloop1

	lda #00
	sta PF0
	sta PF1
	sta PF2
	
	ldx #$07
cwaste2	lda colors,x
	sta COLUBK
	sta WSYNC
	dex
	bne cwaste2

	lda #$00
	sta COLUBK
	
	sta WSYNC

	lda v_81
	cmp #$02
	bne outa
	lda #$00
	sta v_81
;	inc COLUPBK
	jsr scroll2
	sta WSYNC
;	lda #$00
;	sta COLUPBK
	
outa    inc v_81
	sta WSYNC
	rts

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
scroll2:
	ldx #$00
scloop:	clc
	lda v_88,x
	rol
	sta v_88,x

	lda v_90,x
	ror
	sta v_90,x
	
	lda v_98,x
	rol
	sta v_98,x
	
	lda v_a0,x
	ror
	sta v_a0,x
	
	ror
	ror
	ror
	ror

	lda v_a8,x
	ror
	sta v_a8,x

	lda v_b0,x
	rol
	sta v_b0,x

	lda v_b8,x
	ror
	sta v_b8,x

	inx
	cpx #$07
	bne scloop
	inc v_82
	lda v_82
	cmp #$08
	bne weiter
	lda #$00
	sta v_82
	
	ldx #$00
	ldy #$00
	lda (v_84),y
	asl
	asl
	asl
	tay

tloop	lda charset,y
	sta v_88,x
	iny
	inx
	cpx #$07
	bne tloop

	inc v_84
weiter	
	rts
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
tuffinit
;	lda #$10
;	sta v_80
	ldx #48+5+8
	lda #$00
-	sta v_81-1,x
	dex
	bne -
	lda #<text
	sta v_84
	lda #>text
	sta v_84+1
	rts
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 	;*=$f900
	+align256
sinx1
 	!binary "tuffscroll/sintab1"
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	+align256
;*=$fa00
	!ct "tuffscroll/topet.ct" {
text:
!text "yo mutherfuckerz this is street tuff of trsi roxxing da house"
!text "    "
!text "i think nobody tried before to do such a smart scroller in the playfield "
!text "registerz   thatz phunny   the whole kode was done in less than a day "
!text "   believe me or not   its no easy task         "
	}
!byte $ff

;*=$fd00
colors:
!byte $90,$91,$92,$93,$94,$95,$96,$97


;*=$fe00
charset:

!byte %00111110
!byte %01100011
!byte %01100011
!byte %01111111
!byte %01100011
!byte %01100011
!byte %01100011
!byte %00000000

!byte %01111110
!byte %01100011
!byte %01100011
!byte %01111110
!byte %01100011
!byte %01100011
!byte %01111110
!byte %00000000

!byte %00111110
!byte %01100011
!byte %01100000
!byte %01100000
!byte %01100000
!byte %01100011
!byte %00111110
!byte %00000000

!byte %01111110
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01111110
!byte %00000000

!byte %01111111
!byte %01100011
!byte %01100000
!byte %01111100
!byte %01100000
!byte %01100011
!byte %01111111
!byte %00000000

!byte %01111111
!byte %01100011
!byte %01100000
!byte %01111100
!byte %01100000
!byte %01100000
!byte %01100000
!byte %00000000

!byte %00111110
!byte %01100011
!byte %01100000
!byte %01100111
!byte %01100011
!byte %01100011
!byte %00111110
!byte %00000000

!byte %01100011
!byte %01100011
!byte %01100011
!byte %01111111
!byte %01100011
!byte %01100011
!byte %01100011
!byte %00000000

!byte %00111100
!byte %00011000
!byte %00011000
!byte %00011000
!byte %00011000
!byte %00011000
!byte %00111100
!byte %00000000

!byte %01111111
!byte %01100011
!byte %00000011
!byte %00000011
!byte %01100011
!byte %01100011
!byte %01111110
!byte %00000000

!byte %01100011
!byte %01100011
!byte %01100110
!byte %01111100
!byte %01100110
!byte %01100011
!byte %01100011
!byte %00000000

!byte %01100000
!byte %01100000
!byte %01100000
!byte %01100000
!byte %01100000
!byte %01100011
!byte %01111111
!byte %00000000

!byte %01100011
!byte %01110111
!byte %01111111
!byte %01101011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %00000000

!byte %01100011
!byte %01110011
!byte %01111011
!byte %01111111
!byte %01101111
!byte %01100111
!byte %01100011
!byte %00000000

!byte %00111110
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %00111110
!byte %00000000

!byte %01111110
!byte %01100011
!byte %01100011
!byte %01111110
!byte %01100000
!byte %01100000
!byte %01100000
!byte %00000000

!byte %00111110
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01101111
!byte %00111111
!byte %00000011

!byte %01111110
!byte %01100011
!byte %01100011
!byte %01111110
!byte %01100011
!byte %01100011
!byte %01100011
!byte %00000000

!byte %00111110
!byte %01100011
!byte %01100000
!byte %00111110
!byte %00000011
!byte %01100011
!byte %00111110
!byte %00000000

!byte %01111111
!byte %00011100
!byte %00011100
!byte %00011100
!byte %00011100
!byte %00011100
!byte %00011100
!byte %00000000

!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %00111110
!byte %00000000

!byte %01100011
!byte %01100011
!byte %01100011
!byte %01100011
!byte %01110111
!byte %00111110
!byte %00011100
!byte %00000000

!byte %01100011
!byte %01100011
!byte %01101011
!byte %01111111
!byte %01111111
!byte %01110111
!byte %01100011
!byte %00000000

!byte %01100011
!byte %01100011
!byte %00110110
!byte %00011100
!byte %00110110
!byte %01100011
!byte %01100011
!byte %00000000

!byte %01100011
!byte %01100011
!byte %01100011
!byte %00111111
!byte %00000011
!byte %01100011
!byte %01111110
!byte %00000000

!byte %01111111
!byte %01100011
!byte %00000110
!byte %00011100
!byte %00110000
!byte %01100011
!byte %01111111
!byte %00000000

!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00011000
!byte %00011000
!byte %00000000

!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00011000
!byte %00011000
!byte %00001000
!byte %00000000

!byte %00011000
!byte %00011000
!byte %00011000
!byte %00011000
!byte %00000000
!byte %00011000
!byte %00011000
!byte %00000000

!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000
!byte %00000000


;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

