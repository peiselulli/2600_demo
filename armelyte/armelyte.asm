	;muster XXX
	am_pf0_first = ram_start
	am_pf1_first = ram_start+1
	am_pf2_first = ram_start+2
	am_pf0_last = ram_start+3
	am_pf1_last = ram_start+4
	am_pf2_last = ram_start+5

	;muster -XX
	am_pf0_first_2 = ram_start+6
	am_pf1_first_2 = ram_start+7
	am_pf2_first_2 = ram_start+8
	am_pf0_last_2 = ram_start+9
	am_pf1_last_2 = ram_start+10
	am_pf2_last_2 = ram_start+11

	;muster X--
	am_pf0_first_3 = ram_start+12
	am_pf1_first_3 = ram_start+13
	am_pf2_first_3 = ram_start+14
	am_pf0_last_3 = ram_start+15
	am_pf1_last_3 = ram_start+16
	am_pf2_last_3 = ram_start+17

	am_zp_zero = ram_start+22

	am_pos = ram_start+24
	am_phase = ram_start+25
        am_star_pos = ram_start+26
	am_current_sprite = ram_start+27

	am_var = ram_start+28
	am_var_pos = 0
        am_var_phase = 1
        am_var_move = 2
	am_var_y = 3
	am_var_y_pos = 4
        am_var_end = 5
        
        am_var_0 = am_var
        am_var_1 = am_var+am_var_end
        am_var_2 = am_var+am_var_end*2
        am_var_3 = am_var+am_var_end*3
        am_var_4 = am_var+am_var_end*4
        ;am_var_5 = am_var+am_var_end*5
	
;am_pause0:
;	sty PF1
;	sty PF2
;	bit $ea
;	nop
;	nop
;	nop
;	nop
;	nop
;	rts

armelyte:
	lda initflag
	bne armelyte_noinit
	lda #$80
	sta part_duration
	lda #$04
	sta am_pos
	lda #$00
	sta+1 am_zp_zero
	lda #$10
	sta+1 am_zp_zero+1
        lda #$01
        sta VDELP0
        lda #17
        sta am_var_0+am_var_pos
        asl
        asl
        sta am_var_0+am_var_move
        lda #48
        sta am_var_1+am_var_pos
	sta am_var_1+am_var_y_pos
        asl
        asl
        sta am_var_1+am_var_move
        lda #38
        sta am_var_2+am_var_pos
	sta am_var_2+am_var_y_pos
        asl
        asl
        sta am_var_2+am_var_move
        lda #16
        sta am_var_3+am_var_pos
	sta am_var_3+am_var_y_pos
        asl
        asl
        sta am_var_3+am_var_move
        lda #56
        sta am_var_4+am_var_pos
	sta am_var_4+am_var_y_pos
        asl
        asl
        sta am_var_4+am_var_move
;        lda #40
;        sta am_var_5+am_var_pos
;	sta am_var_4+am_var_y_pos
;        asl
;        asl
;        sta am_var_5+am_var_move
        lda #$00
        sta am_var_0+am_var_phase
        lda #$14
        sty am_var_1+am_var_phase
        lda #$10
        sta am_var_2+am_var_phase
        lda #$0c
        sta am_var_3+am_var_phase
        lda #$08
        sta am_var_4+am_var_phase
;        lda #$04
;        sta am_var_5+am_var_phase
        lda #$30
        sta CTRLPF
armelyte_noinit:
        lda am_star_pos
        clc
        adc #$05
        cmp #$a8
        bcc +
        lda #$08
+       sta am_star_pos
	ldx #$04
	jsr PosObject
        sta WSYNC
        sta HMOVE
        sta WSYNC
        lda #$80
        sta HMBL
	ldx #am_var_0
        jsr am_move_ship
	ldx #am_var_1
        jsr am_move_ship
	ldx #am_var_2
        jsr am_move_ship
	ldx #am_var_3
        jsr am_move_ship
	ldx #am_var_4
        jsr am_move_ship
;	ldx #am_var_5
;        jsr am_move_ship
	jsr WaitForVblankEnd

	ldx #am_var_0
	jsr am_set_am_pos
	jsr paint_armelyte_triple

	ldx #am_var_1
	jsr am_set_am_pos
	jsr paint_armelyte_double

	ldx #am_var_2
	jsr am_set_am_pos
	jsr paint_armelyte_triple

	ldx #am_var_3
	jsr am_set_am_pos
	jsr paint_armelyte_single

	ldx #am_var_4
	jsr am_set_am_pos
	jsr paint_armelyte_triple
        ldx #$08
-	sta WSYNC
	dex
        bne -
        sta HMOVE
        sta WSYNC
        lda #$ff
        sta ENABL
        sta WSYNC
        lda #$00
        sta ENABL        
        rts

;	ldx #am_var_5
;	jsr am_set_am_pos
;	jmp paint_armelyte_double

am_set_am_pos:
	lda am_var_pos,x
	sta am_pos
	lda am_var_phase,x
	sta am_phase
	lda am_var_y_pos,x
	asr #$0f*2
	tay
	lda am_y_movetab,y
	sta am_var_y,x
	stx am_current_sprite
	rts

am_move_ship:
	inc am_var_move,x
	lda am_var_pos,x
	cmp #3
	bcc ++
	cmp #15
	bcs ++
        lda am_var_move,x
        ;tay
        and #$71
        bne +
        ;tya
        ;and #$01
        ;bne +
++	dec am_var_pos,x
        lda am_var_pos,x
        cmp #$f0
        bne +
        lda #40
        sta am_var_pos,x
	jmp ++
+
	inc am_var_y_pos,x
++
	lda am_var_phase,x
        clc
        adc #$01
        cmp #7*4
        bne +
        lda #$00
+	sta am_var_phase,x
	rts
                
armelyte_0:
	!src "armelyte/code0.asm"
armelyte_1:
	!src "armelyte/code1.asm"
armelyte_2:
	!src "armelyte/code2.asm"

	+align256
paint_armylyte_sub0:
	ldx #$00
        lda am_pos
        asl
        asl
	adc #$12
	jsr PosObject
	ldx #$01
        lda am_pos
        asl
        asl
	adc #$12
	jsr PosObject
	ldx #$02
        lda am_pos
        asl
        asl
	adc #$0f
	jsr PosObject
	ldx #$03
        lda am_pos
        asl
        asl
	adc #$10
	jsr PosObject
	sta WSYNC
	sta HMOVE
        lda am_pos
        cmp #$22
        bcs +
        sta WSYNC
        cmp #$13
        bcs +
        sta WSYNC
        cmp #$12
        bcs +
        sta WSYNC
        sta WSYNC
+	rts

paint_armelyte_pause1:
	ldx am_current_sprite
	ldy am_var_y,x
	beq +
-	sta WSYNC
	dey
	bne -
+	rts

paint_armelyte_make_pause:
	ldx #12
-	sta WSYNC
	dex
        bne -
        sta WSYNC
        sta HMOVE
        lda #$02
        sta ENABL
        sta WSYNC
        sta HMOVE
        lda #$00
        sta ENABL
	jsr paint_armelyte_pause1
	ldx #22
-	sta WSYNC
	dex
        bne -
paint_armelyte_pause2:
	ldx am_current_sprite
	lda #$04
	sec
	sbc am_var_y,x
	tay
	beq +
-	sta WSYNC
	dey
	bne -
+	rts
                
paint_armelyte_single_before:
	clc
        adc #$08
        sta am_pos
        !byte $2c
paint_armelyte_single:
	sta WSYNC
	lda am_pos
        bmi paint_armelyte_make_pause               
        cmp #38
        bcs paint_armelyte_make_pause
	jsr paint_armylyte_sub0
	ldy am_pos
	jsr set_am_sprite_single
        jmp set_am_cont

paint_armelyte_double_before:
	clc
        adc #$08
        sta am_pos
        !byte $2c
paint_armelyte_double:
	sta WSYNC
	lda am_pos
        bmi paint_armelyte_single_before               
        cmp #38
        bcs paint_armelyte_make_pause
	jsr paint_armylyte_sub0
	ldy am_pos
	cpy #30
	bcc +
	jsr set_am_sprite_single
        jmp set_am_cont
+	jsr set_am_sprite_dual
        jmp set_am_cont
        
paint_armelyte_triple:
	sta WSYNC
	lda am_pos
        bmi paint_armelyte_double_before
        cmp #38
        bcs paint_armelyte_make_pause
	jsr paint_armylyte_sub0
	ldy am_pos
        cpy #30
        bcc +
	jsr set_am_sprite_single
        jmp set_am_cont
+	cpy #22
	bcc +
	jsr set_am_sprite_dual
        jmp set_am_cont
+	jsr set_am_sprite_tripel
                        
set_am_cont:
	sta WSYNC                
        lda #$02
        sta ENABL
	lda am_pos
	cmp #37
	bcs +
	lda #$70
	!byte $2c
+	lda #$50
	sta HMP0
	sta HMP1
	sta HMM0
	sta HMM1
	sta WSYNC
	sta HMOVE
        lda #$00
        sta ENABL
	jsr paint_armelyte_pause1
	lda am_phase
	lsr
	lsr
	tax

	;in x phase
	lda am_jmp_table_low,x
	sta tmp1
	lda am_jmp_table_high,x
	sta tmp1+1
        ldx #$00
	jmp (tmp1)

jmp_armelyte_0:
	+jsr armelyte_0
	jmp paint_armelyte_pause2

jmp_armelyte_1:
	+jsr armelyte_1
	jmp paint_armelyte_pause2

jmp_armelyte_2:
	+jsr armelyte_2
	jmp paint_armelyte_pause2

jmp_armelyte_3:
	+jsr armelyte_3
	jmp paint_armelyte_pause2

jmp_armelyte_4:
	+jsr armelyte_4
	jmp paint_armelyte_pause2

jmp_armelyte_5:
	+jsr armelyte_5
	jmp paint_armelyte_pause2

jmp_armelyte_6:
	+jsr armelyte_6
	jmp paint_armelyte_pause2

am_jmp_table_low:
	!byte <jmp_armelyte_0
	!byte <jmp_armelyte_1
	!byte <jmp_armelyte_2
	!byte <jmp_armelyte_3
	!byte <jmp_armelyte_4
	!byte <jmp_armelyte_5
	!byte <jmp_armelyte_6

am_jmp_table_high:
	!byte >jmp_armelyte_0
	!byte >jmp_armelyte_1
	!byte >jmp_armelyte_2
	!byte >jmp_armelyte_3
	!byte >jmp_armelyte_4
	!byte >jmp_armelyte_5
	!byte >jmp_armelyte_6

	!macro set_am_sprite .table, .value1, .value2, .value3 {
        lda .table,y
        sta+1 .value3
        ora .table+1,y
        ora .table+2,y
        sta+1 .value1
        eor .table,y
        sta+1 .value2
        }
        
set_am_sprite_single:
	lda #$00
	sta NUSIZ0
	lda #$10
	sta NUSIZ1
	sta WSYNC
	+set_am_sprite pf0_last_table, am_pf0_last, am_pf0_last_2, am_pf0_last_3
	+set_am_sprite pf1_last_table, am_pf1_last, am_pf1_last_2, am_pf1_last_3
	+set_am_sprite pf2_last_table, am_pf2_last, am_pf2_last_2, am_pf2_last_3
	+set_am_sprite pf0_first_table, am_pf0_first, am_pf0_first_2, am_pf0_first_3
	+set_am_sprite pf1_first_table, am_pf1_first, am_pf1_first_2, am_pf1_first_3
	+set_am_sprite pf2_first_table, am_pf2_first, am_pf2_first_2, am_pf2_first_3
	rts

set_am_sprite_dual:
	lda #$02
	sta NUSIZ0
	lda #$12
	sta NUSIZ1
	sta WSYNC
	+set_am_sprite dual_pf0_last_table, am_pf0_last, am_pf0_last_2, am_pf0_last_3
	+set_am_sprite dual_pf1_last_table, am_pf1_last, am_pf1_last_2, am_pf1_last_3
	+set_am_sprite dual_pf2_last_table, am_pf2_last, am_pf2_last_2, am_pf2_last_3
	+set_am_sprite dual_pf0_first_table, am_pf0_first, am_pf0_first_2, am_pf0_first_3
	+set_am_sprite dual_pf1_first_table, am_pf1_first, am_pf1_first_2, am_pf1_first_3
	+set_am_sprite dual_pf2_first_table, am_pf2_first, am_pf2_first_2, am_pf2_first_3
	rts

set_am_sprite_tripel:
	lda #$06
	sta NUSIZ0
	lda #$16
	sta NUSIZ1
	sta WSYNC
	+set_am_sprite tripel_pf0_last_table, am_pf0_last, am_pf0_last_2, am_pf0_last_3
	+set_am_sprite tripel_pf1_last_table, am_pf1_last, am_pf1_last_2, am_pf1_last_3
	+set_am_sprite tripel_pf2_last_table, am_pf2_last, am_pf2_last_2, am_pf2_last_3
	+set_am_sprite tripel_pf0_first_table, am_pf0_first, am_pf0_first_2, am_pf0_first_3
	+set_am_sprite tripel_pf1_first_table, am_pf1_first, am_pf1_first_2, am_pf1_first_3
	+set_am_sprite tripel_pf2_first_table, am_pf2_first, am_pf2_first_2, am_pf2_first_3
        rts

	+align256
pf0_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
pf0_first_table:
	!byte $10,$20,$40,$80,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
pf1_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
pf1_first_table:
	!byte $00,$00,$00,$00,$80,$40,$20,$10,$08,$04,$02,$01,$00,$00,$00,$00,$00,$00,$00,$00
pf2_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
pf2_first_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$02,$04,$08,$10,$20,$40,$80
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
        !byte $00,$00


dual_pf0_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$10,$20,$40,$80,$00,$00,$00,$00
dual_pf0_first_table:
	!byte $10,$20,$40,$80,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
dual_pf1_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$80,$40,$20,$10
dual_pf1_first_table:
	!byte $08,$04,$02,$01,$80,$40,$20,$10,$08,$04,$02,$01,$00,$00,$00,$00,$00,$00,$00,$00
dual_pf2_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
dual_pf2_first_table:
	!byte $00,$00,$00,$00,$01,$02,$04,$08,$10,$20,$40,$80,$01,$02,$04,$08,$10,$20,$40,$80
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
        !byte $00,$00

tripel_pf0_last_table:
	!byte $00,$00,$00,$00,$10,$20,$40,$80,$00,$00,$00,$00,$10,$20,$40,$80,$00,$00,$00,$00
tripel_pf0_first_table:
	!byte $10,$20,$40,$80,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
tripel_pf1_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$80,$40,$20,$10,$08,$04,$02,$01,$80,$40,$20,$10
tripel_pf1_first_table:
	!byte $08,$04,$02,$01,$80,$40,$20,$10,$08,$04,$02,$01,$00,$00,$00,$00,$00,$00,$00,$00
tripel_pf2_last_table:
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$02,$04,$08
tripel_pf2_first_table:
	!byte $10,$20,$40,$80,$01,$02,$04,$08,$10,$20,$40,$80,$01,$02,$04,$08,$10,$20,$40,$80
	!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
        !byte $00,$00

am_y_movetab:
	!byte $02,$03,$03,$04,$04,$04,$03,$03,$02,$01,$01,$00,$00,$00,$01,$01

