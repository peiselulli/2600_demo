#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define H 12

unsigned char pixels[H][40];
unsigned char bytes[6*H];

unsigned char bits[40] =
	{ 4,5,6,7,
          15,14,13,12,11,10,9,8,
          16,17,18,19,20,21,22,23,
	  4+24,5+24,6+24,7+24,
          15+24,14+24,13+24,12+24,11+24,10+24,9+24,8+24,
          16+24,17+24,18+24,19+24,20+24,21+24,22+24,23+24};
          
void calc_wave(double altitude, int offset)
{
	int i,j;
        double wave;
        memset(pixels, 0, sizeof(pixels));
        for(i=0; i < 40; ++i)
        {
        	wave = sin(((double)(i+offset))/20.0 * M_PI) * altitude + 7.5;
                for(j=1; j < (int)wave; ++j)
                	pixels[j-1][i] = 1;
                //printf("%d\n", (int)wave);
        }
        for(i=0; i < 40; ++i)
        {
        	for(j=0; j < H; ++j)
                {
        		pixels[j][i] =
                        	1 - pixels[j][i];
                }
        }
        printf("----------------------------------------\n");
}

void print_wave(void)
{
	int i,j;
        for(j=0; j < H; ++j)
        {
        	for(i=0; i < 40; ++i)
                	putchar(pixels[j][i] ? '#' : ' ');
                putchar('\n');
        }
}

void calc_bitmask(void)
{
	int i,j, offset, bit;
        unsigned char *adr;
        memset(bytes, 0, sizeof(bytes));
        for(j=0; j < H; ++j)
        {
        	adr = bytes+j*6;
        	for(i=0; i < 40; ++i)
        	{
			if(pixels[j][i])
                        {
                        	offset = bits[i] / 8;
                                bit = bits[i] & 7;
                                adr[offset] |= (1 << bit);
                        }
        	}
        }
}

void save_bitmask(FILE *f)
{
	fwrite(bytes, 6, H, f);
}

int main(void)
{
	int i,j;
        FILE *f;
        f = fopen("waves", "wb");
        for (i=1; i < 7 ; ++i)
        {
        	for(j=0; j < 20; j+=8)
                {
        		calc_wave((double)(i), j);
			print_wave();
                        calc_bitmask();
                        save_bitmask(f);
                }
        }
        fclose(f);
        return 0;
}
