	wave_screen = ram_start	;6*12 Byte = 72 Bytes
        wave1 = ram_start+72
        wave2 = ram_start+73
        wave3 = ram_start+74
        wave4 = ram_start+75
        wave5 = ram_start+76
        color1 = ram_start+77
        color2 = ram_start+78
        current_sprite = ram_start+79
        spritey1 = ram_start+80 
        spritey2 = ram_start+81 
        spritey3 = ram_start+82 
        spritey4 = ram_start+83 
        spritey5 = ram_start+84 
        spritex1 = ram_start+85 
        spritex2 = ram_start+86 
        spritex3 = ram_start+87 
        spritex4 = ram_start+88 
        spritex5 = ram_start+89 
        spritex1l = ram_start+90 
        spritex2l = ram_start+91 
        spritex3l = ram_start+92 
        spritex4l = ram_start+93 
        spritex5l = ram_start+94 
        
	;initcode in "waves2.asm"
        
no_init_waves:
	;sta WSYNC        
        lda #$7a
        ;sta color1
        ldy #$78
	;sta color2

	ldx wave1
        jsr makewave
	sta WSYNC
        lda #$78
        ;sta color1
        ldy #$76
	;sta color2
        ldx wave2
        jsr makewave

	sta WSYNC
        lda #$76
        ;sta color1
        ldy #$74
	;sta color2
	ldx wave3
        jsr makewave

	sta WSYNC
        lda #$74
        ;sta color1
        ldy #$72
	;sta color2
	ldx wave4
        jsr makewave

	sta WSYNC
        lda #$72
        tay
	;sta color1
	;sta color2
	ldx wave5
        jsr makewave
        rts
        
makewave:
	sta color1
        sty color2       
        txa
        and #$e0
        lsr
        sta tmp1
        txa
        and #$1f
        tax
        lda waveform,x
        php
        and #$7f
        ora tmp1
        tax
        lda wavetable,x
        sta tmp1
        lda wavetable+1,x
        sta tmp1+1
        plp
        bmi copy_negative

        ldy #72-1
-	lda (tmp1),y
        eor #$ff
	sta wave_screen,y
        dey
	lda (tmp1),y
        eor #$ff
	sta wave_screen,y
        dey
	lda (tmp1),y
        eor #$ff
	sta wave_screen,y
        dey
	lda (tmp1),y
        eor #$ff
	sta wave_screen,y
        dey
	lda (tmp1),y
        eor #$ff
	sta wave_screen,y
        dey
	lda (tmp1),y
        eor #$ff
	sta wave_screen,y
        dey
        bpl -
        jmp copy_positive

!macro copy_negative .d {
	!for k, 6 {
	ldy #k-1+(11-.d)*6
        lda (tmp1),y
        sta wave_screen+.d*6+k-1
        }
;        dey
;        bpl -
;        lda tmp1
;        clc
;        adc #$06
;        sta tmp1
;        bcc +
;        inc tmp1+1
;+ 	      
	}
        
copy_negative:
	+copy_negative 11
	+copy_negative 10
	+copy_negative 9
	+copy_negative 8
	+copy_negative 7
	+copy_negative 6
	+copy_negative 5
	+copy_negative 4
	+copy_negative 3
	+copy_negative 2
	+copy_negative 1
	+copy_negative 0
        sta WSYNC
        
        
copy_positive:
-       sta WSYNC
	lda INTIM
        bne -
        lda #11
        sta TIM64T
        ldy current_sprite
        ldx #$01
        lda spritex1,y
        jsr PosObject
	;lda #$00
        ;sta COLUBK
        ldy current_sprite
        lda spritey1,y
        tay
        sta WSYNC
	sta HMOVE
	lda spritedat,y
        sta GRP1
        lda wv_colors,y
        sta COLUP1
        iny
-	sta WSYNC
        sta HMCLR
	lda spritedat,y
        sta GRP1
        lda wv_colors,y
        sta COLUP1
        iny
	lda INTIM
        bne -

	sta WSYNC
	lda spritedat,y
        sta GRP1
        lda wv_colors,y
        sta COLUP1
        iny
        nop

        ldx #72-6
-
	sta WSYNC
	lda spritedat,y
        sta GRP1
	lda wave_screen,x
        sta PF0
        lda wave_screen+1,x
        sta PF1
        lda wv_colors,y
        sta COLUP1
        lda wave_screen+2,x
        sta PF2
	lda wave_screen+3,x
        sta PF0
        lda wave_screen+4,x
        sta PF1
        lda wave_screen+5,x
        sta PF2
        iny
        txa
        sbx #$06
        bpl -
        ldx #$00
	stx PF0
	lda #$00
        sta GRP1
        lda color1
        sta COLUBK
        lda color2
        sta COLUPF
        stx PF1
        stx PF2
        lda #22
        sta TIM64T
        
        inc current_sprite
	rts	        
        
wavedat:
	!for i, 6 {
        !byte 0,0,0,0,0,0
        }
	!for i, 6 {
        !byte $ff,$ff,$ff,$ff,$ff,$ff
        }
        
	!bin "waves/waves"

wavetable:
	!word  wavedat
        !word  wavedat+(3)*72
        !word  wavedat+(3+3)*72
        !word  wavedat+(3+3+3)*72
        !word  wavedat+(3+3+3+3)*72
        !word  wavedat+(3+3+3+3+3)*72
        !word  wavedat+(3+3+3+3+3+3)*72
        !word 0

	!word  wavedat
        !word  wavedat+(2)*72
        !word  wavedat+(2+3)*72
        !word  wavedat+(2+3+3)*72
        !word  wavedat+(2+3+3+3)*72
        !word  wavedat+(2+3+3+3+3)*72
        !word  wavedat+(2+3+3+3+3+3)*72
        !word 0

	!word  wavedat
        !word  wavedat+(1)*72
        !word  wavedat+(1+3)*72
        !word  wavedat+(1+3+3)*72
        !word  wavedat+(1+3+3+3)*72
        !word  wavedat+(1+3+3+3+3)*72
        !word  wavedat+(1+3+3+3+3+3)*72
        !word 0

spritedat:
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %..#####.
        !byte %.#######
        !byte %########
        !byte %###...##
        !byte %##......
        !byte %##......
        !byte %##......
        !byte %##......
        !byte %##......
        !byte %##......
        !byte %##......
        !byte %###...##
        !byte %########
        !byte %.#######
        !byte %..#####.
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........

spritedat1:
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %######..
        !byte %#######.
        !byte %########
        !byte %##...###
        !byte %##....##
        !byte %##...###
        !byte %#######.
        !byte %######..
        !byte %#####...
        !byte %##.###..
        !byte %##..##..
        !byte %##..###.
        !byte %##...##.
        !byte %##...###
        !byte %##....##
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        
spritedat2:
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %..######
        !byte %.#######
        !byte %########
        !byte %###.....
        !byte %##......
        !byte %##......
        !byte %#######.
        !byte %#######.
        !byte %#######.
        !byte %##......
        !byte %##......
        !byte %###.....
        !byte %########
        !byte %.#######
        !byte %..######
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........

spritedat3:
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %..####..
        !byte %.######.
        !byte %########
        !byte %###...##
        !byte %##......
        !byte %###.....
        !byte %.###....
        !byte %..###...
        !byte %....###.
        !byte %.....###
        !byte %......##
        !byte %##...###
        !byte %########
        !byte %.######.
        !byte %..####..
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........

spritedat4:
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %########
        !byte %########
        !byte %########
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %...##...
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........
        !byte %........

wv_colors:
	!for i, 5 {
        !for i, 13 {
        !byte $80+(i)
        }
        !for i, 14 {
        !byte $60+(14-i)
        }
	}
                      
waveform:
	!byte $0,$2,$4,$6,$8,$a,$a,$c,$c,$c,$a,$a,$8,$6,$4,$2,$0
        !byte $82,$84,$86,$88,$8a,$8a,$8c,$8c,$8c,$8a,$88,$86,$84,$82,0
waveformlength = * - waveform
        
