waves:
	lda initflag
        bne do_no_init_waves
	lda #$60
	sta part_duration
        ldx #$05-1
-	lda #$9e
	sta spritex1,x
        dex
        bpl -
        lda #$00
        sta PF0
        sta PF1
        sta PF2
        lda #$00
        sta wave1
        lda #$08
        sta wave2
        lda #$25
        sta wave3
        lda #$4d
        sta wave4
        lda #$53
        sta wave5
        lda #$04
        ldx #$00
        jsr PosObject
        lda #$a0
        ldx #$02
        jsr PosObject
        sta WSYNC
        sta HMOVE
        sta WSYNC
        sta HMCLR
        lda #$30
        sta NUSIZ0        
        sta WSYNC
        sta HMOVE
        lda #$ff
        sta GRP0
        sta ENAM0
        ;lda #$87
        ;sta COLUP1
do_no_init_waves:

	ldx #$00
-       lda wave1,x
	lsr
        and #$1f
        tay
        lda waveform_copy,y
        and #$7f
        lsr
        cmp #$06
        bcc +
        lda #$05
+       
	clc
        adc spritedats,x
        sta spritey1,x
        lda spritex1l,x
        clc
        adc spritespeedtab,x
        sta spritex1l,x
        bcc ++
        lda #$00
        dcp spritex1,x
        bne +
        lda #$a0
        sta spritex1,x
+       lda spritex1,x
	cmp #$0f
        bne ++
        lda #$0c
        sta spritex1,x
++	inx
        cpx #$05
        bne -
        lda #$7a
        sta COLUPF
        lda #$be
	sta COLUBK
 
 	ldx #$04
-	inc wave1,x
        lda wave1,x
        cmp #waveformlength*3
        bne +
        lda #$00
        sta wave1,x
+       dex
	bpl -

        
	;jsr WaitForVblankEnd
        ldx #$00
        stx current_sprite
-	STA WSYNC	
	LDA INTIM	
	BNE -
	STA VBLANK
        lda #29
        sta TIM64T

	+jmp no_init_waves

waveform_copy:
	!byte $0,$2,$4,$6,$8,$a,$a,$c,$c,$c,$a,$a,$8,$6,$4,$2,$0
        !byte $82,$84,$86,$88,$8a,$8a,$8c,$8c,$8c,$8a,$88,$86,$84,$82,0

spritespeedtab:
	!byte $50, $80,$60,$40,$70        
spritedats:
	!byte spritedat-spritedat
	!byte spritedat1-spritedat
	!byte spritedat2-spritedat
	!byte spritedat3-spritedat
	!byte spritedat4-spritedat
