TEMPO = 64

; Set all 8 values to zero
; if you want all sounds at full volume.  10 is probably 
; about the highest amount of attenuation you'll want.
;
; 000=square  001=bass  010=pitfall  011=noise
; 100=buzz    101=lead  110=saw      111=engine
soundVolArray:
	!byte 7,10,6,7,7,10,5,10

; Default Sound Type Setup:
; 000 0 Square  = 4
; 001 1 Bass    = 6
; 010 2 Pitfall = 7
; 011 3 Noise   = 8
;
; 100 4 Buzz    = 15
; 101 5 Lead    = 12
; 110 6 Saw     = 1
; 111 7 Engine  = 3
;
soundTypeArray
    !byte 4,6,7,8
    !byte 15,12,7,3

HATVOLUME = 4
HATPITCH  = 1
HATSOUND  = 8
;HATSTATIC = 1         ;if non-zero, use static hat pattern (saves 5 B)
HATPATTRN = %10000000 ;if HATSTATIC

B5 = %00001111 ;q
C6 = %00001110 ;w
E6 = %00001011 ;t
G6 = %00001001 ;u
A6 = %00001000 ;i
B6 = %00000111 ;1

C3 = %11101111
F3 = %11101011
C4 = %11100111


Eng0a = %11111111
Eng0b = %11111101
Eng0c = %11110111
Eng0d = %11110011
Eng0e = %11110001
Eng1a = %11101111
Eng1b = %11101110
Eng1c = %11101011
Eng1d = %11101001
Eng1e = %11101000
Eng2a = %11100111

Kick0 = %01111111
Slap0 = %01100100
TomHi = %01101011
TomLo = %01101111

;bass
BB2 = %00100111
BE3 = %00100101
BG3 = %00100100
BB3 = %00100011
BE4 = %00100010
BB4 = %00100001

;pitfall (same tuning as bass)
PB2 = %01000111
PE3 = %01000101
PG3 = %01000100
PB3 = %01000011
PE4 = %01000010
PB4 = %01000001

!ifdef HATSTATIC {
} else {
hatPattern
    !byte %10001000
    !byte %10001000
    !byte %10001000
    !byte %00001010
}

    !macro MEASURES .a, .b, .c, .d { ;saves 3 B in player by premultiplying by eight
    !byte .a*8, .b*8, .c*8, .d*8
    }
INTROLENGTH = 8
HATSTART = 4            ;saves 6 B if == 0 (@ song1)
    !src "song/measuretab.asm"

patternArray    ; starts at 1
    !word EBassDa,EBassDb,EBassDc,EBassDb
    !word EBassDe,EBassDb,EBassDc,EBassDb
    !word EBassBa,EBassBb,EBassBc,EBassBb
    !word EBassBe,EBassBb,EBassBc,EBassBb
    !word EBassCa,EBassCb,EBassCc,EBassCb
    !word EBassCe,EBassCb,EBassCc,EBassCb
    !word EBassEa,EBassEb,EBassEc,EBassEb
    !word EBassEe,EBassEb,EBassEc,EBassEb
    !word EBassAa,EBassAb,EBassAc,EBassAb
    !word EBassAe,EBassAb,EBassAc,EBassAb
    !word Kick0a,0,Kick0a,0
    !word Kick0b,0,Kick0b,0
    !word MelBG,MelE3,MelE3,MelG3
    !word MelB2,MelE3,MelG3,MelE3
    !word MelB3,MelG3,MelE3,MelG3
    !word MelB3,Me2G3,MelE3,MelG3
    !word Me2G3,MelBG,MelE3,MelG3
    !word MelBG,MelBG,MelE3,MelG3
    !word IBassDa,IBassDb,IBassDc,IBassDb
    !word IBassDe,IBassDb,IBassDc,IBassDb
    !word 0,Slide0,Slide1,Slide2
    !word Slide3,SlideE,0,0
    !word 0,0,0,Trans0                       ;23
    !word Twst0a,Twst0b,Twst0c,Twst0d
    !word Twst1a,Twst1b,Twst1c,Twst1d
    !word Twst2a,Twst2b,Twst2c,Twst2d
    !word Twst3a,Twst3b,Twst3c,Twst3d
    !word 0,Tom0a,0,Tom0b
    !word 0,0,0,Tom0c

Envelopes
    !byte 15,15,15,15,  15,15,15,15,    15,15,15,15,    15,15,15,15   ;normal
    !byte  0, 1, 2, 3,   4, 5, 6, 7,     8, 9,10,11,    12,13,14,15   ;fade in
    !byte 15,14,13,12,  11,10, 9, 8,     7, 6, 5, 4,     3, 2, 1, 0   ;fade out

Twst0a !byte %00010111,%00010111,%00010111,%00010111,%00010111,%00010111,%00010111,%00010111,    %11110000
Twst0b !byte %00001011,%00001011,%00001011,%00001011,%00001011,%00001011,%00001011,%00001011,    %11100000
Twst0c !byte %00001011,%00001011,%00001011,%00001011,%00010111,%00010111,%00010111,%00010111,    %00001100
Twst0d !byte %00001011,%00001011,%00001011,%00001011,%00010111,%00010111,%00010111,%00010111,    %11001100
Twst1a !byte %00010011,%00010011,%00010011,%00010011,%00010011,%00010011,%00010011,%00010011,    %11110000
Twst1b !byte %00001001,%00001001,%00001001,%00001001,%00001001,%00001001,%00001001,%00001001,    %11100000
Twst1c !byte %00001001,%00001001,%00001001,%00001001,%00010011,%00010011,%00010011,%00010011,    %00001100
Twst1d !byte %00001001,%00001001,%00001001,%00001001,%00010011,%00010011,%00010011,%00010011,    %11001100
Twst2a !byte %00011111,%00011111,%00011111,%00011111,%00011111,%00011111,%00011111,%00011111,    %11110000
Twst2b !byte %00001111,%00001111,%00001111,%00001111,%00001111,%00001111,%00001111,%00001111,    %11100000
Twst2c !byte %00001111,%00001111,%00001111,%00001111,%00011111,%00011111,%00011111,%00011111,    %00001100
Twst2d !byte %00001111,%00001111,%00001111,%00001111,%00011111,%00011111,%00011111,%00011111,    %11001100
Twst3a !byte %00011101,%00011101,%00011101,%00011101,%00011101,%00011101,%00011101,%00011101,    %11110000
Twst3b !byte %00001110,%00001110,%00001110,%00001110,%00001110,%00001110,%00001110,%00001110,    %11100000
Twst3c !byte %00001110,%00001110,%00001110,%00001110,%00011101,%00011101,%00011101,%00011101,    %00001100
Twst3d !byte %00001110,%00001110,%00001110,%00001110,%00011101,%00011101,%00011101,%00011101,    %11001100



Slide0 !byte %10100000,%10100000,%10100001,%10100001,%10100010,%10100010,%10100011,%10100011,    %11111111
Slide1 !byte %10100100,%10100100,%10100101,%10100101,%10100110,%10100110,%10100111,%10100111,    %11111111
Slide2 !byte %10101000,%10101000,%10101001,%10101001,%10101010,%10101010,%10101011,%10101011,    %11111111
Slide3 !byte %10101100,%10101100,%10101101,%10101101,%10101110,%10101110,%10101111,%10101111,    %11111111
SlideE !byte %10101110,%10101111,%10101110,%10101111,%10101110,%10101111,%10101110,%10101111,    %11110000

Trans0 !byte %01101011,%01101011,%01101011,%01101011,%01101011,%01101011,%01100111,%01100111,     %00001110

MelB2 !byte PB2,PB2,BB2,BB2,BB2,BB2,BB2,BB2,%11100000
MelE3 !byte PE3,PE3,BE3,BE3,BE3,BE3,BE3,BE3,%11100000
MelG3 !byte PG3,PG3,BG3,BG3,BG3,BG3,BG3,BG3,%11000000
Me2G3 !byte PG3,BG3,BG3,BG3,PG3,PG3,BG3,BG3,%10001100
MelB3 !byte PB3,PB3,BB3,BB3,BB3,BB3,BB3,BB3,%11000000
MelBG !byte PB3,PB3,BB3,BB3,PG3,BG3,BG3,BG3,%11001100

IBassDa !byte Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,%00001100
IBassDb !byte Eng1d,Eng1d,Eng1d,Eng1d,Eng1d,Eng1d,Eng1d,Eng1d,%11001100
IBassDc !byte Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,%11000000
IBassDe !byte Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,Eng0d,%11001100

;TODO: these are all transpositions - we could save ROM by modifying the player
EBassBa !byte TomLo,TomLo,TomLo,TomLo,Eng0b,Eng0b,Eng0b,Eng0b,%11001100
EBassBb !byte Eng1b,Eng1b,Eng1b,Eng1b,Eng0b,Eng0b,Eng0b,Eng0b,%11001100
EBassBc !byte Eng0b,Eng0b,Eng0b,Eng0b,TomLo,TomLo,TomLo,TomLo,%11001100
EBassBe !byte Eng0b,Eng0b,Eng0b,Eng0b,TomHi,TomHi,TomHi,TomHi,%00001100
EBassCa !byte TomLo,TomLo,TomLo,TomLo,Eng0c,Eng0c,Eng0c,Eng0c,%11001100
EBassCb !byte Eng1c,Eng1c,Eng1c,Eng1c,Eng0c,Eng0c,Eng0c,Eng0c,%11001100
EBassCc !byte Eng0c,Eng0c,Eng0c,Eng0c,TomLo,TomLo,TomLo,TomLo,%11001100
EBassCe !byte Eng0c,Eng0c,Eng0c,Eng0c,TomHi,TomHi,TomHi,TomHi,%00001100
EBassDa !byte TomLo,TomLo,TomLo,TomLo,Eng0d,Eng0d,Eng0d,Eng0d,%10101100
EBassDb !byte Eng1d,Eng1d,Eng1d,Eng1d,Eng0d,Eng0d,Eng0d,Eng0d,%11001100
EBassDc !byte Eng0d,Eng0d,Eng0d,Eng0d,TomLo,TomLo,TomLo,TomLo,%11001100
EBassDe !byte Eng0d,Eng0d,Eng0d,Eng0d,TomHi,TomHi,TomHi,TomHi,%00001100
EBassEa !byte TomLo,TomLo,TomLo,TomLo,Eng0e,Eng0e,Eng0e,Eng0e,%10101100
EBassEb !byte Eng1e,Eng1e,Eng1e,Eng1e,Eng0e,Eng0e,Eng0e,Eng0e,%11001100
EBassEc !byte Eng0e,Eng0e,Eng0e,Eng0e,TomLo,TomLo,TomLo,TomLo,%11001100
EBassEe !byte Eng0e,Eng0e,Eng0e,Eng0e,TomHi,TomHi,TomHi,TomHi,%00001100
EBassAa !byte TomLo,TomLo,TomLo,TomLo,Eng1a,Eng1a,Eng1a,Eng1a,%10101100
EBassAb !byte Eng2a,Eng2a,Eng2a,Eng2a,Eng1a,Eng1a,Eng1a,Eng1a,%11001100
EBassAc !byte Eng1a,Eng1a,Eng1a,Eng1a,TomLo,TomLo,TomLo,TomLo,%11001100
EBassAe !byte Eng1a,Eng1a,Eng1a,Eng1a,TomHi,TomHi,TomHi,TomHi,%00001100

Kick0a !byte Kick0,Kick0,Kick0,Kick0,Kick0,Kick0,Kick0,Kick0,%11110000
Kick0b !byte Kick0,Kick0,Kick0,Kick0,Slap0,Slap0,Slap0,Slap0,%11101000
Tom0a  !byte TomLo,TomLo,TomLo,TomLo,TomLo,TomLo,TomLo,TomLo,%11000000
Tom0b  !byte TomLo,TomLo,TomLo,TomLo,TomHi,TomHi,TomHi,TomHi,%11001100
Tom0c  !byte TomLo,TomLo,TomLo,TomLo,Kick0,Kick0,Kick0,Kick0,%11001100


bitMaskArray
    !byte %10000000
    !byte %01000000
    !byte %00100000
    !byte %00010000
    !byte %00001000
    !byte %00000100
    !byte %00000010
    !byte %00000001
