song1
    +MEASURES  0, 0, 0, 0    ;0
    +MEASURES 11,11,11,11    ;4
    +MEASURES  0, 0, 0, 0    ;8
    +MEASURES  0, 0, 0, 0    ;12
    +MEASURES  0, 0, 0, 0    ;16
    +MEASURES  0, 0, 0,23    ;20
    +MEASURES  0, 0, 0, 0    ;24
    +MEASURES  0, 0, 0, 0    ;28
    +MEASURES  0, 0, 0,23    ;32
    +MEASURES  0, 0, 0, 0    ;36
    +MEASURES 24,25,26,27    ;40
    +MEASURES 25,24,26,27    ;44
    +MEASURES  0, 0, 0, 0    ;48
    +MEASURES  0, 0, 0, 0    ;52
    +MEASURES  0, 0, 0, 0    ;56
    +MEASURES  0, 0, 0, 0    ;60
    +MEASURES  0, 0, 0, 0    ;64
    +MEASURES 12, 0,12, 0    ;68
    +MEASURES 12, 0,12, 0    ;72
    +MEASURES 12, 0,12, 0    ;76
    +MEASURES 12, 0,12, 0    ;80
    +MEASURES 14,15,14,16    ;84
    +MEASURES 14,15,14,13    ;88
    +MEASURES 14,17,14,16    ;92
    +MEASURES 14,18,16,13    ;96
    +MEASURES  0, 0, 0, 0    ;100
    +MEASURES 24,25,26,27    ;104
    +MEASURES 25,24,26,27    ;108
    +MEASURES  0, 0, 0, 0    ;112
    +MEASURES  0, 0, 0, 0    ;116
    +MEASURES  0,28, 0,28    ;120
    +MEASURES 29,28,29,28    ;124
    +MEASURES 29,28,29,23    ;128
    +MEASURES  0, 0, 0, 0    ;132
    +MEASURES  0, 0, 0, 0    ;136
    +MEASURES  0, 0, 0, 0    ;140
    +MEASURES  0, 0, 0,23    ;144
    +MEASURES  0, 0, 0, 0    ;148
    +MEASURES  0, 0, 0, 0    ;152
    +MEASURES  0, 0, 0, 0    ;156
    +MEASURES  0, 0, 0, 0    ;160
    +MEASURES  0, 0, 0, 0    ;164
    +MEASURES  0, 0, 0, 0    ;168
    +MEASURES  0,21,22, 0    ;172
song2
    +MEASURES 19,20,19,20    ;0
    +MEASURES 19,20,19,20    ;4
    +MEASURES  1, 2, 1, 2    ;8
    +MEASURES  1, 2, 1, 2    ;12
    +MEASURES  3, 4, 3, 4    ;16
    +MEASURES  1, 2, 1, 2    ;20
    +MEASURES  5, 6, 3, 4    ;24
    +MEASURES  1, 2, 1, 2    ;28
    +MEASURES 11,11, 1, 2    ;32
    +MEASURES 11,12,11,12    ;36
    +MEASURES 11, 0,11, 0    ;40
    +MEASURES 11, 0,11, 0    ;44
    +MEASURES 11,12,11,12    ;48
    +MEASURES  9,10, 7, 8    ;52
    +MEASURES  5, 6, 5, 6    ;56
    +MEASURES  3, 4, 3, 4    ;60
    +MEASURES  5, 6,11,11    ;64
    +MEASURES 14,15,14,16    ;68
    +MEASURES 14,15,14,13    ;72
    +MEASURES 14,17,14,16    ;76
    +MEASURES 14,18,16,13    ;80
    +MEASURES 14,15,14,16    ;84
    +MEASURES 14,15,14,13    ;88
    +MEASURES 14,17,14,16    ;92
    +MEASURES 14,18,16,13    ;96
    +MEASURES 11,12,11,12    ;100
    +MEASURES 11, 0,11, 0    ;104
    +MEASURES 11, 0,11, 0    ;108
    +MEASURES  0, 0,11,11    ;112
    +MEASURES 12,11,12,11    ;116
    +MEASURES 12,11,12,11    ;120
    +MEASURES 12,11,12,11    ;124
    +MEASURES 12,11,12,11    ;128
    +MEASURES  1, 2, 1, 2    ;132
    +MEASURES  3, 4, 3, 4    ;136
    +MEASURES  1, 2, 1, 2    ;140
    +MEASURES  5, 6,11,11    ;144
    +MEASURES  9,10, 7, 8    ;148
    +MEASURES  5, 6, 5, 6    ;152
    +MEASURES  3, 4, 3, 4    ;156
    +MEASURES  5, 6,11,11    ;160
    +MEASURES  5, 6, 3, 4    ;164
    +MEASURES  1, 2, 1, 2    ;168
    +MEASURES  1, 2, 1, 2    ;172
SONGEND = 176
env1
    !byte   0    ;0
    !byte   0    ;4
    !byte   0    ;8
    !byte   0    ;12
    !byte   0    ;16
    !byte   0    ;20
    !byte   0    ;24
    !byte   0    ;28
    !byte   0    ;32
    !byte   0    ;36
    !byte   0    ;40
    !byte   0    ;44
    !byte   0    ;48
    !byte   0    ;52
    !byte   0    ;56
    !byte   0    ;60
    !byte   0    ;64
    !byte   0    ;68
    !byte   0    ;72
    !byte   0    ;76
    !byte   0    ;80
    !byte  16    ;84
    !byte   0    ;88
    !byte   0    ;92
    !byte  32    ;96
    !byte   0    ;100
    !byte   0    ;104
    !byte   0    ;108
    !byte   0    ;112
    !byte   0    ;116
    !byte   0    ;120
    !byte   0    ;124
    !byte   0    ;128
    !byte   0    ;132
    !byte   0    ;136
    !byte   0    ;140
    !byte   0    ;144
    !byte   0    ;148
    !byte   0    ;152
    !byte   0    ;156
    !byte   0    ;160
    !byte   0    ;164
    !byte   0    ;168
    !byte   0    ;172
env2
    !byte   0    ;0
    !byte   0    ;4
    !byte   0    ;8
    !byte   0    ;12
    !byte   0    ;16
    !byte   0    ;20
    !byte   0    ;24
    !byte   0    ;28
    !byte   0    ;32
    !byte   0    ;36
    !byte   0    ;40
    !byte   0    ;44
    !byte   0    ;48
    !byte   0    ;52
    !byte   0    ;56
    !byte   0    ;60
    !byte   0    ;64
    !byte   0    ;68
    !byte   0    ;72
    !byte   0    ;76
    !byte   0    ;80
    !byte   0    ;84
    !byte   0    ;88
    !byte   0    ;92
    !byte  32    ;96
    !byte   0    ;100
    !byte   0    ;104
    !byte   0    ;108
    !byte   0    ;112
    !byte   0    ;116
    !byte   0    ;120
    !byte   0    ;124
    !byte   0    ;128
    !byte   0    ;132
    !byte   0    ;136
    !byte   0    ;140
    !byte   0    ;144
    !byte   0    ;148
    !byte   0    ;152
    !byte   0    ;156
    !byte   0    ;160
    !byte   0    ;164
    !byte   0    ;168
    !byte   0    ;172
