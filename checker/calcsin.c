#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int i;
        int v;
        double a;
        FILE *f;
        
        f = fopen("sintab", "wb");
	for (i=0; i < 64; ++i)
        {
        	a = sin((double)i / 32.0 * M_PI) * 15.0;
                a +=16.5;
                v = (int)a;
                fputc(v, f);
        }
        fclose(f);
}
