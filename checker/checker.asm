	ch_col0 = tmp1
	ch_col1 = tmp2
        ch_old_y = ram_start
        ch_stack_merk = ram_start+1
	ch_state_low = ram_start+2
	ch_size = ram_start+3
        ch_angle = ram_start+4
        ch_size_add = ram_start+5
        ch_angle_value = ram_start+6
        
        ch_ptr1 = ch_size
        ch_ptr2 = ch_size+2
        
        ch_buffer = ram_start+7

CH_COLOR_EOR = $80
CH_TIME = $a8
CH_TIME2 = $58

NUMBER_OF_LINES = 93

checker:
	sta WSYNC
	sta WSYNC
	sta WSYNC
	lda #21
        sta T1024T
	lda initflag
        bne checker_no_init
	sta ch_state_low
	sta ch_size
        sta ch_size_add
        sta ch_angle
	lda #<CH_TIME
	sta part_duration
	;lda #$00
	;sta COLUPF
	;lda #$02
	;sta ENABL
	lda #$33
	sta CTRLPF
checker_no_init:
	lda initflag
        and #$0f
        bne ++
	lda part_duration
        cmp #CH_TIME-$20
        bne +
        inc ch_size_add
+       cmp #CH_TIME-$31
	bne +
        dec ch_size_add
        dec ch_size_add        
+	cmp #CH_TIME-$3c
	bne +
        inc ch_size_add
        inc ch_size_add        
+	cmp #CH_TIME-$50
	bne +
        dec ch_size_add        
+
++	+skip_framewait
        stx ch_stack_merk

	lda part_duration
        cmp #CH_TIME-$50
        bcs +
        cmp #CH_TIME-$70
        bcc +
        inc ch_angle
        bne +
        dec ch_angle
+
        cmp #CH_TIME-$7f
        bcs +
        cmp #CH_TIME-$a0
        bcc +
        dec ch_angle
        bne +
        inc ch_angle
+	jsr ch_paint_chess
ch_checker1:
        lda #$ff
        sta GRP0
        sta GRP1
        ldx #$00
        stx ENAM0
        stx ENAM1
        stx PF0
        stx PF1
        stx PF2
        lda #$30
        sta NUSIZ0
        sta NUSIZ1
        ldy ch_buffer
	lda sp0array,y
        jsr PosObject
        inx
        ldy ch_buffer
	lda sp1array,y
        jsr PosObject        
        inx
	lda #$20
	jsr PosObject
        inx
	lda #$88
	jsr PosObject
	sta WSYNC
	sta HMOVE
	lda #$00
	sta COLUBK	
	sta COLUP0
	sta COLUP1
	lda #$00
	STA VBLANK

        ldx #ch_buffer
        txs
        sta HMCLR
        sta WSYNC
        sta HMCLR
        ldy #$09
        lda ch_buffer
        and #$3f
        tax
-	cpx #07
	bcc fine_spr
        lda #$70
        sta HMM0
        lda #$90
        sta HMM1
        sta WSYNC
        sta HMOVE
        lda (00,x)
        lda (00,x)
        lda (00,x)
        lda (00,x)
        txa
        sbx #$07
        dey
        bne -
        jmp wait_for_sync
fine_spr:
	lda ch_movenega_tab,x
        sta HMM0
        lda ch_moveposi_tab,x
        sta HMM1
        sta WSYNC
        sta HMOVE
        lda (00,x)
        lda (00,x)
        lda (00,x)
        lda (00,x)
        dey
        beq wait_for_sync
-	sta HMCLR        
        sta WSYNC
	dey
        bne -        
wait_for_sync:
        sta WSYNC
        sta HMCLR
-       lda INTIM
        cmp #$0e
        bne -
        sta WSYNC
        ldy ch_buffer        
	sta WSYNC
        ldx ch_enatab_left,y
        stx ENAM0
        ldx ch_enatab_left,y
        stx ENAM1
	lax ch_colortab,y
	eor #CH_COLOR_EOR
	sta WSYNC
	ldy #10
-	dey
	bne -
        ldy ch_buffer        
        jmp +

	;+align256
ch_mainloop:
	sta HMOVE
	sta COLUBK
        stx COLUP0
        sta COLUP1        
        lda pf0array,y
        sta PF0
        lda pf1array,y
        sta PF1
        ldx ch_enatab_left,y
        stx ENAM0
        lda pf2array,y
        sta PF2
        sty ch_old_y
        ldx ch_enatab_right,y
        stx ENAM1
        lda ch_col1
	sta COLUBK
        ldx sp0array,y
        pla
        tay
        txa
        sbc sp0array,y
        sec
	sta HMCLR
	sta HMOVE
        tax
	lda ch_col0
	sta COLUBK
        lda ch_movenega_tab,x
        sta HMP0
        lda ch_moveposi_tab,x
        sta HMP1
        tya
        sbc ch_old_y
	tax
        lda ch_movenega_tab,x
        sta HMM0
        lda ch_moveposi_tab,x
        sta HMM1
	lda ch_col1
	sta COLUBK
        lax ch_colortab,y
        eor #CH_COLOR_EOR
+       sec
        sta ch_col0
        stx ch_col1
	dec INTIM	
	bpl ch_mainloop

	LDA #2		
	STA VBLANK
	STA WSYNC  	
        lda #40
        sta TIM64T
        ;inc ch_buffer
        ;inc ch_buffer
        ;inc ch_buffer
        ;inc ch_buffer
        ;inc ch_buffer+1
        ;inc ch_buffer+1
        ;inc ch_buffer+1
        ;inc ch_buffer+2
        ;inc ch_buffer+2
        ;inc ch_buffer+3        
        ldx ch_stack_merk
        txs
	rts
        
ch_paint_chess:
        lda ch_size
        cmp #$08
        bcs +
        lda #$08
+       sta tmp1
	lda ch_state_low
	asl
	sta tmp2 
 	ldx #$00
	bcc +
	ldx #$40
+
	txa
	ora tmp1
	tax
	lda #$80
	sta tmp1
        sta ch_angle_value

	clc
	ldy #NUMBER_OF_LINES-1
ch_calc_loop:
	stx ch_buffer,y
	lda tmp1
	adc ch_size_tab_low,x
	sta tmp1
	lda tmp2
	adc ch_size_tab,x
	sta tmp2
	bcc +
	txa
	eor #$40
	tax
	clc
+
	lda ch_angle_value
        adc ch_angle
        sta ch_angle_value
        bcc +
        lda ch_new_angle,x
        bmi ch_do_entry2
        clc
        tax
+	dey
	bpl ch_calc_loop
ch_cont:lax ch_state_low
	sbx #$04
	stx ch_state_low
	txa
	and #$0f
	bne +
	lda ch_size
        clc
        adc ch_size_add
	and #$3f
	sta ch_size
+	rts

ch_do_entry2:
	txa
        eor #$40
        tax
        jmp ch_entry2
        
ch_calc_loop2:
	stx ch_buffer,y
	lda tmp1
	adc ch_size_tab_low,x
	sta tmp1
	lda tmp2
	sbc ch_size_tab,x
	sta tmp2
	bcs +
	txa
	eor #$40
	tax
	sec
+
	lda ch_angle_value
        adc ch_angle
        sta ch_angle_value
        bcc +
ch_entry2:
        inx
+	sec
	dey
	bpl ch_calc_loop2
        jmp ch_cont


checker2:
	sta WSYNC
	sta WSYNC
	sta WSYNC
	lda #21
        sta T1024T
	lda initflag
        bne checker_no_init2
        !ifdef enable_checker {
        } else {
	sta ch_state_low
        }
        sta ch_ptr1
        sta ch_ptr2
        lda #>sintab
        sta ch_ptr1+1
        sta ch_ptr2+1
	lda #<CH_TIME2
	sta part_duration
	;lda #$00
	;sta COLUPF
	;lda #$02
	;sta ENABL
	lda #$33
	sta CTRLPF
checker_no_init2:
	+skip_framewait
        stx ch_stack_merk
        jsr ch_paint_chess2
        jmp ch_checker1

ch_paint_chess2:
	lda ch_state_low
	asl
	sta tmp2 
 	ldx #$00
	bcc +
	ldx #$40
+
	lda #$80
	sta tmp1

	clc
	ldy #NUMBER_OF_LINES-1
ch_calc_loop_sin:
	txa
        and #$c0
	adc (ch_ptr1),y
        adc (ch_ptr2),y
+       tax
	stx ch_buffer,y
	lda tmp1
	adc ch_size_tab_low,x
	sta tmp1
	lda tmp2
	adc ch_size_tab,x
	sta tmp2
	bcc +
	txa
	eor #$40
	tax
	clc
+	dey
	bpl ch_calc_loop_sin
	lax ch_state_low
	sbx #$04
	stx ch_state_low
        dec ch_ptr1
        inc ch_ptr2
	lda initflag
        lsr
        bcc +
        inc ch_ptr2
+	rts
        
	+align128
pf0array:
	!for j, 4 {
	!for i, 16 {
        !byte $f0
        }
        !for i, 4 {
        !byte $70
        }
        !for i, 4 {
        !byte $30
        }
        !for i, 4 {
        !byte $10
        }
        !for i, 4 {
        !byte $00
        }
        !for i, 32 {
        !byte 0
        }
	}
        
pf1array:
	!for j, 4 {
	!for i, 4 {
        !byte $e0
        }
        !for i, 4 {
        !byte $c0
        }
        !for i,4 {
        !byte $80
        }
        !for i,12 {
        !byte $00
        }
        !for i,8 {
        !byte $01
        }
        !for i,8 {
        !byte $03
        }
        !for i,8 {
        !byte $07
        }
        !for i,8 {
        !byte $0f
        }
        !for i,8 {
        !byte $1f
        }
        }
pf2array:
	!for j, 4 {
	!for i, 10 {
        !byte $fc
        }
	!for i, 16 {
        !byte $fe
        }
	!for i, 32+6 {
        !byte $ff
        }
        }


sp0array:
	!for j, 4 {
	!for i, $20 {
        !byte $3f-(i-1),$3f-(i-1)
        }
        }
sp1array:
	!for j, 4 {
	!for i, $20 {
        !byte $67+(i-1),$67+(i-1)
        }
        }

ch_moveposi_tab:
	!for j, 4 {
	!for i, 8 {
        !byte <($100-(i-1)*$10)
        }
        !for i, 64-8-7 {
        !byte 0
        }
        !for i, 7 {
        !byte (8-i)*$10
        }
        }

ch_movenega_tab:
	!for j, 4 {
        !for i, 8 {
        !byte (i-1)*$10
        }
        !for i, 64-8-7 {
        !byte 0
        }
	!for i, 7 {
        !byte <($100-(8-i)*$10)
        }
        }

ch_enatab_right:
	!for j,4 {
        !for i,$20 {
        	!byte $02
        }
        !for i,$20 {
        	!byte $00
        }
        }

	+align256
ch_enatab_left:
	!for j,4 {
        !for i,$20-7 {
        	!byte $02
        }
        !for i,$20+7 {
        	!byte $00
        }
        }

ch_colortab:
	!for i, 7 {
        !for j, 9 {
        !if (i=1) & (j = 1) {
        !byte $00
        } else {
	!byte $2e-(16-(i*2))
        }
        }
        }
	!byte $22
	!for i, 7 {
        !for j, 9 {
        !if (i=1) & (j = 1) {
        !byte $00
        } else {
	!byte $ae-(16-(i*2))
        }
        }
        }
	!byte $a2

        
ch_size_tab:
	!for j, 2 {
	!byte 10,10,10,10,10,10
	!byte 09,09,09,09,09,09,09
	!byte 08,08,08,08,08,08,08,08
	!byte 07,07,07,07,07,07,07,07,07
	!byte 06,06,06,06,06,06,06,06,06,06
	!byte 05,05,05,05,05,05,05,05,05,05,05
	!byte 04,04,04,04,04,04,04,04,04,04,04,04,$04
	}
        
ch_size_tab_low:
	!for j,2 {
	!for i, 6 {
	!byte 256*(6-i)/6
	}
	!for i, 7 {
	!byte 256*(7-i)/7
	}
	!for i, 8 {
	!byte 256*(8-i)/8
	}
	!for i, 9 {
	!byte 256*(9-i)/9
	}
	!for i, 10 {
	!byte 256*(10-i)/10
	}
	!for i, 11 {
	!byte 256*(11-i)/11
	}
	!for i, 12 {
	!byte 256*(12-i)/12
	}
        !byte $00
        }

ch_new_angle:
	!byte $ff
        !for i, 63 {
        !byte i-1
        }
        !byte $ff
        !for i, 63 {
        !byte $40+(i-1)
        }

sintab:
	!for i, 6 {
        !bin "checker/sintab"
        }
                


