SHELL = /bin/bash

DIRS = sinbar vector twister waves ball crystal checker checker2 \
        logo sprite_multiplier armelyte scroll startpic

all:
	DIRS="$(DIRS)" make main

include Makefiles/rules

test:
	@for i in $(DIRS); do make -C $$i; done;

flash:
	mount /media/flashcard
	cp main.bin /media/flashcard
	umount /media/flashcard
        
